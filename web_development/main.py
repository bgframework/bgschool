import os
import cherrypy
from jinja2 import Environment, FileSystemLoader


directory = os.path.dirname(os.path.abspath(__file__))


class WebApp(object):

    def __init__(self, environment):
        self.env = environment

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect('home')

    @cherrypy.expose
    def home(self):
        return 'My home page'

    @cherrypy.expose
    def show(self, name, surname=None):
        user_surname = '' if surname is None else surname
        return 'Hi {} {}'.format(name, user_surname)

    @cherrypy.expose
    def basic_html(self):
        file = open(os.path.join(directory, 'html', 'simple.html'))
        return file.read()  # return a string

    @cherrypy.expose
    def other(self):
        return 'other page'

    @cherrypy.expose
    def form_example(self):
        file = open(os.path.join(directory, 'html', 'simple_form.html'))
        return file.read()  # return a string

    @cherrypy.expose
    def styled_paragraphs(self):
        file = open(os.path.join(directory, 'html', 'paragraphs.html'))
        return file.read()  # return a string

    @cherrypy.expose
    def bootstrap(self):
        file = open(os.path.join(directory, 'html', 'bootstrap.html'))
        return file.read()

    @cherrypy.expose
    def js(self):
        file = open(os.path.join(directory, 'html', 'javascript.html'))
        return file.read()

    @cherrypy.expose
    def jquery(self):
        file = open(os.path.join(directory, 'html', 'jquery.html'))
        return file.read()

    @cherrypy.expose
    def template(self, names=''):
        return self.env.get_template('greet.html').render(list_of_names=names.split(','))




if __name__ == '__main__':
    env = Environment(loader=FileSystemLoader(os.path.join(directory, 'templates')))
    conf = {
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': os.path.join(directory, 'static', 'css')
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': os.path.join(directory, 'static', 'js')
        }
    }
    cherrypy.quickstart(WebApp(env), '/', conf)
