import os
import cherrypy
from jinja2 import Environment, FileSystemLoader

directory = os.path.dirname(os.path.abspath(__file__))


class WebApp(object):

    def __init__(self, environment):
        self.env = environment

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect('home')

    @cherrypy.expose
    def home(self, names=''):
        return self.env.get_template('home.html').render(list_of_names=names.split(','))


if __name__ == '__main__':
    env = Environment(loader=FileSystemLoader(os.path.join(directory, 'templates')))
    cherrypy.quickstart(WebApp(env))


"""
A template engine takes a template file and some data and renders it to generate
another file.

[[Start without the names and and them and their logic later]]

Templates can be useful to:
- reusing items in other pages 
  (home page only fills the content, the rest is taken from the layout. 
  Take a look at the html code of the home page before rendering and after)
- generate 'code' (markup...) according to some data
  (provide a comma separated list of names to the home page: /home?names=erika,oriol,ines,ferran)

Templates can be useful, but try to remove as much logic as you can from the templates.

Note: templates are not restricted to HTML, you can use wherever you find them useful.

"""