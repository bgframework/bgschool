import os
import cherrypy

directory = os.path.dirname(os.path.abspath(__file__))


class WebApp(object):
    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect('home')

    @cherrypy.expose
    def home(self):
        file = open(os.path.join(directory, 'html', 'jquery.html'))
        return file.read()


if __name__ == '__main__':
    cherrypy.quickstart(WebApp())


"""
jQuery: https://jquery.com/, https://code.jquery.com/
jQuery is a JavaScript library that makes easier to select DOM elements and
handle events amongst other things.

Take a look at the console to see the message appear once the page is ready
and use the button to change the display attribute of a <p>
"""
