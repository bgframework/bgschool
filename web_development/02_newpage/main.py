import cherrypy


class WebApp(object):

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect('home')

    @cherrypy.expose
    def home(self):
        return 'My home page'

if __name__ == '__main__':
    cherrypy.quickstart(WebApp())

"""
HTTPRedirect sends the user to another page -> home

cherrypy.expoxe makes the URL accessible. Try commenting it.
"""