import os
import cherrypy

directory = os.path.dirname(os.path.abspath(__file__))


class WebApp(object):

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect('home')

    @cherrypy.expose
    def home(self):
        file = open(os.path.join(directory, 'html', 'simple_form.html'))
        return file.read()  # return a string

    @cherrypy.expose
    def show(self, name, surname):
        return 'Hi {} {}'.format(name, surname)


if __name__ == '__main__':
    cherrypy.quickstart(WebApp())


"""
HTML form helps to create URL with the parameters.

POST method will hide the parameters in the URL, but they are still there
"""