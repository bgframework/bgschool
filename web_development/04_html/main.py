import os
import cherrypy

directory = os.path.dirname(os.path.abspath(__file__))


class WebApp(object):

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect('home')

    @cherrypy.expose
    def home(self):
        file = open(os.path.join(directory, 'html', 'simple.html'))
        return file.read()  # return a string

    @cherrypy.expose
    def other(self):
        return 'other page'


if __name__ == '__main__':
    cherrypy.quickstart(WebApp())


"""
Check basic elements of an HTML file.

- HEAD: information about the page, title, stylesheets... (and JavaScript)
- BODY: components of the page (and JavaScript)

- tag elements with <> & </>
- self-closed items (</>)
- items can have attributes (id, ...)
- items can be nested

    - headers (h1, h2...)
    - paragraphs (div, p)
    - span (to add style)
     - table (table, tr, th, td)
     - list (ul, li)
     - hyperlink (a)
     - text edit (b, i)
     - line break (br)
"""