import cherrypy


class WebApp(object):

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect('home')

    @cherrypy.expose
    def home(self):
        return 'My home page'

    @cherrypy.expose
    def show(self, name, surname=None):
        user_surname = '' if surname is None else surname
        return 'Hi {} {}'.format(name, user_surname)


if __name__ == '__main__':
    cherrypy.quickstart(WebApp())

"""
Parameters are passed through the URL using the format ?param=value

Check what happens when addressing:
- /show
- /show?name=bbglab
- show?nname=bbglab
- show?name=Brian&surname=Cohen
- show?name=Brian&surname=Cohen&other=av
"""
