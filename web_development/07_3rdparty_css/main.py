import os
import cherrypy

directory = os.path.dirname(os.path.abspath(__file__))


class WebApp(object):

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect('home')

    @cherrypy.expose
    def home(self):
        file = open(os.path.join(directory, 'html', 'bootstrap.html'))
        return file.read()  # return a string

    @cherrypy.expose
    def other(self):
        return 'other page'

if __name__ == '__main__':
    conf = {
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': os.path.join(directory, 'static', 'css')
        }
    }
    cherrypy.quickstart(WebApp(), '/', conf)


"""
See that the style of the button is different from the default
and we have not defined the btn or btn-primary classes

Inspect div and disable/enable style and check priority order
"""