import cherrypy


class WebApp(object):

    @cherrypy.expose
    def index(self):
        return "Hello World!"


if __name__ == '__main__':
    cherrypy.quickstart(WebApp())

"""
http://docs.cherrypy.org/en/latest/tutorials.html#tutorial-1-a-basic-web-application

IP address: number for identification  (DNS allows to use strings)
Port: used to identify services

Index is the default entry point of any web application

Check HTML code in the browser. Is not only a string!!
"""
