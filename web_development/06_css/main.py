import os
import cherrypy

directory = os.path.dirname(os.path.abspath(__file__))


class WebApp(object):

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect('home')

    @cherrypy.expose
    def home(self):
        file = open(os.path.join(directory, 'html', 'paragraphs.html'))
        return file.read()  # return a string

if __name__ == '__main__':
    conf = {
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': os.path.join(directory, 'static', 'css')
        }
    }
    cherrypy.quickstart(WebApp(), '/', conf)


"""
Notice the static files directory,
the link to the stylesheet in the HTML
and check the URL: /css/basic.css

Priority of CSS is set by specificity.

Style can be indicated by any of the selectors
https://www.w3schools.com/cssref/css_selectors.asp
"""