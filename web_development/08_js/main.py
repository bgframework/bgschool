import os
import cherrypy

directory = os.path.dirname(os.path.abspath(__file__))


class WebApp(object):
    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect('home')

    @cherrypy.expose
    def home(self):
        file = open(os.path.join(directory, 'html', 'javascript.html'))
        return file.read()


if __name__ == '__main__':
    conf = {
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': os.path.join(directory, 'static', 'js')
        }
    }
    cherrypy.quickstart(WebApp(), '/', conf)


"""
See that the Javascript can be embedded in the HTML
or loaded from a static file.

Javascript code is executed in the client side.
"""
