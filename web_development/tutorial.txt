Python web development
======================

i) Web application

   - 2 parts:

     - client: normally a web browser that renders and displays the web content
     - server: stores, process and deliver web pages. Runs the application to generate the page.

   - client and server communicate using HTTP protocol (application layer) + TCP/IP.

     - GET: request a resource
     - POST: create a resource
     - PUT: modifies a resource
     - DELETE: removes a resource
     - ...

   - the server runs any code it understands (Pyhton, Java...)
     and returns HTML (+ CSS + JavaScript)

     - HTML: components of your page
     - CSS: style of those components
     - JavaScript: code executed on the client side

   - 2 types of applications:

     - traditional: multiple pages that user navigates through
     - single page: all-in-one

   - to serve static HTML:
     - python -m http.server


ii) Djanjo vs. cherrypy

   - WSGI: interface spec. between server and application
   - Web framework (full stack vs. micro-framework)
   - Development vs. production server
   - 4 pages Hello World vs. 6 lines
   - Huge community (plugins...) vs. small community


iii) Basic tutorials 1-9

   1. Cherrypy Hello World
   2. Adding a new page
   3. URL parameters
   4. First HTML
   5. HTML form to send parameters
   6. CSS intro
   7. Using others CSS
   8. Javascript intro
   9. jQuery

iv) Intermediate tutorials 10-

   10. Templates
   11. SCSS
   12. Avoid caching
   13. Ajax
   14. Websockets
   15. Authentication
   16. Cookies
   17. Session
