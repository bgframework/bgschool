import os
import mmap
from functools import lru_cache

CHR_PATH = "/projects_bg/bg/soft/intogen_home/gencluster/software/mutsigCV/reffiles/chr_files_hg19"

def get_signature_hg19_v01(chromosome, start, delta=1):
    with open(os.path.join(CHR_PATH, "chr{0}.txt".format(chromosome)), 'r+b') as hg19:
        hg19.seek(start - 1 - delta)
        return hg19.read(1 + delta * 2).decode().upper()
    
def get_signature_hg19_v02(chromosome, start, delta=1):
    with open(os.path.join(CHR_PATH, "chr{0}.txt".format(chromosome)), 'r+b') as fd:
        hg19 = mmap.mmap(fd.fileno(), 0)
        hg19.seek(start -1 - delta)
        return hg19.read(1 + delta * 2).decode().upper()

@lru_cache(maxsize=40)
def get_hg19_mmap(chromosome):
    fd = open(os.path.join(CHR_PATH, "chr{0}.txt".format(chromosome)), 'r+b')
    return mmap.mmap(fd.fileno(), 0)

def get_signature_hg19_v03(chromosome, start, delta=1):
    mm_file = get_hg19_mmap(chromosome)
    mm_file.seek(start -1 - delta)
    return mm_file.read(1 + delta * 2).decode().upper()


