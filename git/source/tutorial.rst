

First steps
===========

.. highlight:: bash

This tutorial is intended to be done in pairs
(named ``A`` and ``B``). Before each set of commands,
the user that should execute the commands is indicated.

Before starting
---------------

You need to have the following tools installed:

.. hlist::
   :columns: 4

   - git
   - git gui
   - gitk
   - meld

If you have never used git before, you should probably need to configure git::

   $ git config [--global] user.name "<your name>"
   $ git config [--global] user.email <your name>@<domain>

You can use any diff tool of your choice. If you want to use meld::

   $ git config [--global] merge.tool meld

.. note::

   Use ``--global`` flag if you are interested in setting those value for all the repositories.


The merge tools may leave backup files after doing a merge
with the extension :file:`.orig`.
You can prevent that with::

   $ git config [--global] mergetool.keepBackup false

However, keep in mind that the mergetool configuration can override this behaviour.
You can also choose to add those files to :file:`.gitignore`.

Initializing the repo
---------------------

We would like to start a project and keep track of the files in it.
Thus, we initialize git in the project folder::

   # A
   $ mkdir <folder name>
   $ cd <folder name>
   $ git init
   Initialized empty Git repository in ...

We can check that now a ``.git`` folder has been created::

   # A
   $ ls -a
   .  ..  .git

That folder is the one that contains all the information for the version control.

Now we can check the status of the repo::

   # A
   $ git status
   On branch master
   Initial commit
   nothing to commit (create/copy files and use "git add" to track)

For now, we only care about the "nothing to commit" message.
It means that git does not find anything to add to version control.

Adding a file
-------------

Now it is time to create a file :file:`hello.py` with a single line:

.. code-block:: python

   print('Hello world, I am <A name>')

And check the status::

   # A
   $ git status
   On branch master
   Initial commit
   Untracked files:
     (use "git add <file>..." to include in what will be committed)
       hello.py
   nothing added to commit but untracked files present (use "git add" to track)

.. _tutorial add:

Now the message has changed, saying that we have "untracked" files.
**Untracked files** are the ones that are not under version control yet.
As we will like to keep this file under version control, we can *add* them
to git::

   # A
   $ git add hello.py

Let's see the current status::

   # A
   $ git status
   On branch master
   Initial commit
   Changes to be committed:
     (use "git rm --cached <file>..." to unstage)
       new file:   hello.py

.. _tutorial commit:

The message has changed again. Now it says that that there are changes to be committed.
What as happened is that the file has gone to the **staging area**.
Git has created a "snapshot" with all the changes from the previous commit
until the moment we have added it.
In this case, as there was not any previous commit, the entire file is
in that "snapshot".
Now git is ready to group all these changes together into one commit::

   # A
   $ git commit -m "Added greetings"
   [master (root-commit) 6e9ec45] Added greetings
    1 file changed, 1 insertion(+)
    create mode 100644 hello.py

.. warning::

   All the commits need to include a message. Please, make it meaningful.


.. _commit hash:

All commits include are uniquely identified by a hash. The ``6e9ec45``
is the first part of the hash, and is normally enough to uniquely identify one commit.

Great! The first commit is done. Let's see the status again::

   # A
   $ git status
   On branch master
   nothing to commit, working tree clean

The *working tree* is clean. That means that all the changes we have done are already in git
and there is no difference between our version and the latest version git knows about.

----

Just to check what has git done, we can take a look at the log::

   # A
   $ git log
   ...


In the log again we can see the full :ref:`hash number of the commit <commit hash>`
together with who did the commit and when and the message.

Using a remote
--------------

So far, only ``A`` has been having fun and ``B`` is getting a bit bored.
``B`` want to collaborate with ``A`` and for that we are going to set up
a repository that both can access::


   # A or B
   $ cd <common location>
   $ mkdir <name>.git
   $ git init --bare
   Initialized empty Git repository in ...

.. note::

   This repo works as you Bitbucket or Github repo.

   The ``--bare`` flags makes a repo that only contains the changes
   but not the actual files, so you won't be able to find any of your files
   in it.

Now ``A`` and ``B`` have a "common" place, but both need to address it.

.. _tutorial add origin:

Lets start with ``A``.
First of all, ``A`` should indicate its git repo that there is a remote in that location::

   # A
   $ git remote add origin <folder>

.. _tutorial push:

Now ``A``'s local remote is linked to the remote one that has been named as *origin* [#origin]_.
Both repositories are linked, but we still need to synchronize them.
To do so, ``A`` needs to push his/her changes from the local to the remote::


   # A
   $ git push -u origin master
   Counting objects: 3, done.
   Writing objects: 100% (3/3), 254 bytes | 0 bytes/s, done.
   Total 3 (delta 0), reused 0 (delta 0)
   To ...
    * [new branch]      master -> master
   Branch master set up to track remote branch master from origin.

.. _tutorial clone:

Now that ``A`` has make all the changes public, it is time for ``B`` to get them.
First of all, ``B`` needs to *clone* the repo::


   # B
   $ git clone <folder> <name>
   Cloning into ...
   done

Now all the files should also be in ``B``'s repo::

   # B
   $ cd <name>
   $ ls
   hello.py


At this point, ``A`` and ``B`` have their copies of the repo and there is a join point for both as a remote.

First changes
-------------

Now ``B`` is willing to make some changes. ``B`` opens
the :file:`hello.py` file and changes it:

.. code-block:: python

   print('Hello world, we are <A name> and <B name>')

.. _tutorial git gui:

Once it is done, its is time to share the changes with ``A``.
Instead of using the command line, we are going to use the git gui tool::

   # B
   $ git gui

.. image:: /_static/gitgui.png

With the git gui we can commit the changes (with a message) and push them to the remote.
Remember to stage the files to be committed and give a message.

.. _tutorial pull:

Once done, ``A`` can get the changes back::

   # A
   $ git pull
   ...
   Updating 6e9ec45..83012b1
   Fast-forward
    hello.py | 2 +-
    1 file changed, 1 insertion(+), 1 deletion(-)

.. _tutorial diff:

``A`` can see that there has been some changes in the :file:`hello.py` file.
To see what changes has been done, we can use :command:`git diff <hash>`.

.. note::

   The previous hash can be found in the pull message: ``Updating 6e9ec45..83012b1``
   6e9ec45 is the old hash and 83012b1 the new.

::

   # A
   $ git diff 6e9ec45
   diff --git a/hello.py b/hello.py
   index 990773f..a929b33 100644
   --- a/hello.py
   +++ b/hello.py
   @@ -1 +1 @@
   -print('Hello world, I am Iker')
   +print('Hello world, we are Jordi and Iker')



Now we can see what has been changed in the last commit.

Minor conflicts
---------------

We have been lucky that ``A`` and ``B`` were working in different steps,
once one has finished the other has started.
Now we are going to see what happen when both work at the same time
but on different items.


``A`` wants to provide another message in the  :file:`hello.py` file.
``A`` adds the following line:

.. code-block:: python

   print('Good to see you here')

In the meantime, ``B`` has opened :file:`hello.py` with Pycharm
(or another IDE) that generates some temporary files.
When ``B`` checks the status, there are untracked files::

   # B
   $ git status
   On branch master
   Your branch is up-to-date with 'origin/master'.
   Untracked files:
     (use "git add <file>..." to include in what will be committed)
       .idea/
   nothing added to commit but untracked files present (use "git add" to track)

.. _tutorial gitignore:

Those files that are exclusive of one person's editor should not be under version control.
Thus ``B`` decides to ignore them.
The best way to inform git that some files and folder should be ignored is using
a :ref:`.gitignore <gitignore file>`::

   # Pycharm files
   .idea


And both commit those changes using either the :ref:`git gui <tutorial git gui>` tool or
the command line (via :ref:`add <tutorial add>` + :ref:`commit <tutorial commit>`).

At the time of pushing, one will be faster that the other.
Lets suppose ``B`` pushes the changes first::

   # B
   $ git push
   ...


Nice and clean. But what happens to ``A``?::

   # A
   $ git push
   To ...
   ! [rejected]        master -> master (fetch first)
   error: failed to push some refs to ...


Oh! It has not worked. What has happened?
Once ``B`` has published the changes, the "baseline" of the remote has changed
to be the one of ``B``. Then, when it tries to apply ``A``'s changes,
as the baseline is different and we get an error.

So ``A`` has to solve it.

.. note::

   Conflicts are never solved in the remote repository.
   First they must be solved in your local copy and then
   you can spread you changes.

::

   # A
   $ git pull
   ...

In the result message it is said: "Merge made by the 'recursive' strategy."
This means that git has done the merging for you.

Now ``A`` can push the changes::

   # A
   $ git push
   ...

.. hint::

   Be sure you are up-to-date with the remote before making any push.


----

.. _tutorial gitk:

Now we can take a look at the history making us to the ``gitk`` tool::

   # A
   $ gitk


.. image:: /_static/gitk.png

Major conflicts
---------------

In the previous example we have seen what could happen
when two developers work at the same time.
The lesson to be learn from that example is that
we need to be sure to be updated before applying changes.

Previously, we were lucky that we
did not found too much trouble.
But what would have happened if both developers were editing the same files?
Lets see it.

To try to avoid issues as before,
``A`` and ``B`` want to be sure they are up-do-date
before continue working::

   # A and B
   $ git pull
   ...


Now ``A`` wants to change the second line in :file:`hello.py`:

.. code-block:: python

   print('Glad to see you here')

while ``B`` has a similar idea:

.. code-block:: python

   print('Happy to see you here')

Both commit these changes but now it is ``A`` the one pushing them faster::

   # A
   $ git push

While ``B`` gets now intro troubles::

   # B
   $ git push
   To ...
   ! [rejected]        master -> master (fetch first)
   error: failed to push some refs to ...


In the previous case, git has done the merge for us. Will it work again?::

   # B
   $ git pull
   ...
   CONFLICT (content): Merge conflict in hello.py
   Automatic merge failed; fix conflicts and then commit the result.

Ouch! Git has not help us this time, ``B`` has to do it
"manually". Now is when tools like ``meld`` come handy::

   # B
   $ git mergetool

.. image:: /_static/meld.png

.. note::

   There are other options to ``meld``. E.g. Pycharm includes a similar one.

.. warning::

   After doing the merge, we need to **commit** and **push**, even if there are no files to be staged.

Using branches
--------------

To make things easier and better, git includes the concept of **branch** [#branch]_.
We are going to explore this concept.

First of all, both developers are going to update their repos::

   # A and B
   $ git pull
   ...

Now developers users are going to create a branch named ``develop``
and switch to it::

   # A
   $ git branch develop
   $ git checkout develop
   Switched to branch 'develop'

   # B
   $ git checkout -b develop
   Switched to a new branch 'develop'


For now on, all the changes we make are going to be in that particular branch.

.. note::

   We can always check what branched do we have and in which one we are::

      # A and B
      $ git branch

      * develop
        master


``A`` changes the :file:`hello.py` file adding:

.. code-block:: python

   print('This is great')

while ``B`` adds:

.. code-block:: python

   print('We are impressed')

Now both commit their changes.
Now if we try to submit those changes, we found that git does not allow us::

   # A and B
   $ git push
   fatal: The current branch develop has no upstream branch.
   To push the current branch and set the remote as upstream, use

       git push --set-upstream origin develop

Basically, the remote does not know yet about this ``develop`` branch,
so it cannot track it.
In addition, we do not want the remote to know about this branch.
It is our particular internal branch and we will publish the changes
in the master branch.

Now ``A`` and ``B`` are happy with their changes and want to
publish them in the master branch. The steps to follow are  [#mergesteps]_:

1. Ensure your working environment is clean
#. Checkout to the master branch
#. Make sure the master branch is up to date
#. Merge with your develop branch (here you might have conflicts)
#. Commit the merge if needed
#. Push your changes

``B`` is doing it before ``A`` this time::

   # B
   $ git status
   On branch develop
   nothing to commit, working tree clean
   $ git checkout master
   Switched to branch 'master'
   Your branch is up-to-date with 'origin/master'.
   $ git pull
   Already up-to-date.
   $ git merge develop
   Updating 47f8ca6..59c5502
   Fast-forward
    hello.py | 1 +
    1 file changed, 1 insertion(+)
   $ git push
   ...


Now it is ``A``'s time::

   # A
   $ git status
   On branch develop
   nothing to commit, working tree clean
   $ git checkout master
   Switched to branch 'master'
   Your branch is up-to-date with 'origin/master'.
   $ git pull
   ...
   Fast-forward
    hello.txt | 1 +
    1 file changed, 1 insertion(+)
   $ git merge develop
   Auto-merging hello.txt
   CONFLICT (content): Merge conflict in hello.txt
   Automatic merge failed; fix conflicts and then commit the result.


Once again we have found some issues, but now we know how to tackle them::


   # A
   $ git mergetool

And after the merge we commit the merge and push.

Finally, as we are done with our ``develop`` branch we can get rid of it::

   # A and B
   $ git branch -d develop
   Deleted branch develop (was d44ceac).


Tags
----

After all this process, we are happy with the status of our code so we are ready to make a release.
For that purpose, we are going to **tag** a particular commit.

First of all, ``B`` is going to get all the changes, tag the last commit with version ``0.1``
and publish our tag::

   # B
   $ git pull
   ...
   Fast-forward
    hello.py | 2 +-
    1 file changed, 1 insertion(+), 1 deletion(-)
   $ git tag 0.1
   $ git push origin 0.1
   ...
    * [new tag]         0.1 -> 0.1


Now ``A`` should be able to see the tag using the ``gitk`` tool::

   # A
   $ git pull
   ...
    * [new tag]         0.1        -> 0.1
   Already up-to-date.
   $ gitk


.. image:: /_static/tag.png


----

.. [#origin] *origin* is the default name for a remote in git.
   However, you can name it as you want.
   In addition, you can have as many remotes as needed with different names.

.. [#branch] A branch is a separate line of development

.. [#mergesteps] The recommendation is to first do the merge on
   the 'develop' branch and then go to the master in order to have
   a cleaner master branch.

