.. Git documentation master file, created by
   sphinx-quickstart on Mon Nov 13 13:58:59 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Git's documentation!
===============================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro

A good cheatsheet can be found in `<https://ndpsoftware.com/git-cheatsheet.html>`_

.. toctree::
   :maxdepth: 1
   :caption: Tutorials:

   tutorial

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
