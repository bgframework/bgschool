
Introduction
============

Git is a **distributed version control** system.

What is version control?
------------------------

Version control systems are a category of software tools that help managing changes to source code over time.

What is git?
------------

Git is a distributed version control system.
Each git repository is associated with a project.
Everyone involved has a copy of the entire repository.
You work locally on your copy and then you share your changes with the others (or part of them).
Although is not required, a central repository is normally used as a join point for everybody.
Bitbuket or Github are normally used for this purpose.

How does it work?
-----------------

In short, you make changes to a set of files, then you decide which of those changes are
going to be added to git in the next commit and finally, you publish your changes.

Terms
-----

.. glossary::

    Repo/repository
        Folder containing all the files and the history of changes made to them

    Commit
       Set of changes you group together and put in your repository

    Branch
       Independent line of development

    Master
       Name of the main branch

    Remote
       Other’s git repository (of the same project)

    Origin
       Name of the main common remote repository


Stages of a file
----------------

Get a new file is create, it is **untracked**.
At this point, you can choose between adding it to git
or ignoring it.
If you decide to **ignore** the file because
you do not want git to know about it,
the best way is to add it to the :ref:`gitignore file <gitignore file>`.
On the other hand, if you decide to add it to git, the file goes through
different stages.
Those stages are the same as for any already tracked file
that you make changes to.

.. image:: /_static/gitstages.png


You have your file in the **workspace**.

You can decide to **stash** it.
This implies to get all the changes done to the file and save
them in a *hidden* place. Then the file goes back to its previous state.

If you want to *add* those changes to git, you need to put in
in the **index** or *staging area*.
In this area, git makes a "snapshot" of the file
with the changes when compared with the previous commit.
This means that if you keep changing the file,
you need to update the "snapshot" or git
will not know about those changes.

Once the file is in the index, you can add it
to the **local repo** by *commiting*
the changes in the "snapshot".

Finally, to make those changes available for others
you need to *push* them to the **remote**.



.. _gitignore file:

The .gitignore
--------------

In git there is a special file :file:`.gitignore`.
This file specifies intentionally untracked files that Git should ignore
E.g.::

   # ignore python cached files
   *.pyc

.. warning::

   This setting will not affect files already tracked by git.

   To stop tracking a file that is currently tracked, use :command:`git rm --cached`

In addition, you can negate a pattern with the ``!`` prefix.
E.g.::

   # ignore generated html files,
   *.html
   # except foo.html which is maintained by hand
   !foo.html


Alternatives
------------

If you are not comfortable with **git** there are several alternatives of version control systems.

For those who prefer a client-server model check `Apache subversion <https://subversion.apache.org/>`_

If you prefer is a distributed system, `Mercurial <https://www.mercurial-scm.org/>`_ can be a good alternative.
It has been regarded as easier to use, however it might be more limited in the thing it can do
(although for a "normal" use it has everything you need).
While git can be seen as tool to create a version-control workflow (each git command is a different tool),
mercurial has a more monolithic approach. Read more in https://stackoverflow.com/questions/35837/what-is-the-difference-between-mercurial-and-git
