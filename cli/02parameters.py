"""
Lets add some parameters.

Click supports 2 types of parameters: options and arguments.

While options are optional arguments are required.

"""

import click


@click.command()
@click.option('--greet')
@click.argument('name')
def cli(greet, name):
    if greet is None:
        greet = 'Hello'
    print(greet + ' ' + name)

# $ python 02parameters.py
# $ python 02parameters.py Iker
# $ python 02parameters.py Iker --greet Hi


if __name__ == '__main__':
    cli()
