"""
Creating your first command.
"""

import click


@click.command()
def cli():
    print('Hello world')

# $ python 01base.py
# $ python 01base.py --help


if __name__ == '__main__':
    cli()
