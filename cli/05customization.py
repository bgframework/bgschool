"""
Lets explore some customization options for options and arguments
"""


import click


"""
### 1
Default values can be taken from environment variables
"""


@click.command()
@click.argument('path', envvar='PATH')
def cli(path):
    print('path: ', path)

# $ python 05customization.py
# $ python 05customization.py /workspace


"""
### 2
Click can check whether file paths exists or not
"""


@click.command()
@click.argument('input', type=click.Path(exists=True))
@click.argument('output', type=click.Path(exists=False))
def cli(input, output):
    print('in: ', input)
    print('out: ', output)

# $ python 05customization.py iker 05customization.py
# $ python 05customization.py 05customization.py iker


"""
### 3
You can use your own validation logic
"""


def validate_human(ctx, param, value):
    if value.startswith('hg'):
        return value
    else:
        raise click.BadParameter('Human genomes should start with hg')


@click.command()
@click.argument('ref', callback=validate_human)
def cli(ref):
    print('ref: ', ref)

# $ python 05customization.py mm10
# $ python 05customization.py hg19


"""
### 4
Options can have a shortcut version
"""


@click.command()
@click.option('--name', '-n')
def cli(name):
    print('Hi ', name)

# $ python 05customization.py -n iker


"""
### 5
In addition, you can provide a variable name
"""


@click.command()
@click.option('--name', '-n', 'username')
def cli(username):
    print('Hi ', username)

# python 05customization.py -n iker


if __name__ == '__main__':
    cli()
