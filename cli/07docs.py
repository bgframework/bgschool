"""
Adding documentation for your command line interface is important.
You should document the commands and each of the options.
"""

import click


"""
### 1
Add a docstring to your commands
"""

@click.command()
def cli():
    """CMD docstring"""
    pass

# $ python 07docs.py --help


"""
### 2
Add a help text to your options

Note that the help text also includes the type of the parameter
"""

@click.command()
@click.option('--name', '-n', help='User name')
@click.option('--value', '-v', type=int, help='User value')
def cli(name):
    """CMD docstring"""
    pass

# $ python 07docs.py --help


"""
### 3
Use meta variables to improve readability

Note that args do not have help strings
"""


@click.command(options_metavar='[USER OPTIONS]')
@click.argument('name', metavar='<USER NAME>')
@click.option('--value', '-v', type=int, metavar='[Int value]', help='User value')
def cli(name, value):
    """CMD docstring"""
    pass

# $ python 07docs.py --help


"""
### 4
Add short help to commands in groups
"""


@click.command(short_help='CMD short help txt')
def cmd():
    """CMD docstring"""
    pass


@click.group()
def cli():
    """Group docstring"""
    pass


cli.add_command(cmd)

# $ python 07docs.py --help
# $ python 07docs.py cmd --help


"""
### 5
Enable the use of -h as help command
"""

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.command(context_settings=CONTEXT_SETTINGS)
def cli():
    """Docstring"""
    pass

# $ python 07docs.py -h


if __name__ == '__main__':
    cli()
