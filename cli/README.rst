
Command Line Interface in Python
================================

This tutorial covers how to set up a
CLI for a Python project using
the click library:

http://click.pocoo.org/

Based on click version 6

Execute the scripts using:
$ python <name>.py [<args>]


Enabling the cmd line
---------------------

To enable the command line for your Python package,
you need to add the following lines to the setup

entry_points={
        'console_scripts': [
            '<name> = <pkg>.<mod>:<cli_f>'
        ]
    }


Where <name> is the name you want to give,
and <pkg>.<mod>:<cli_f> is the path to
the functions with the command line interface.


Bash completion
***************

To enable bash completion read: http://click.pocoo.org/6/bashcomplete/


Further reading
---------------

Implementing custom types
http://click.pocoo.org/6/parameters/#implementing-custom-types

Yes parameters
http://click.pocoo.org/6/options/#yes-parameters

Range options
http://click.pocoo.org/6/options/#range-options

Other prefix chars
http://click.pocoo.org/6/options/#other-prefix-characters

Feature switches
http://click.pocoo.org/6/options/#feature-switches

Chaining multiple commands
http://click.pocoo.org/6/commands/#multi-command-chaining

Making pipelines
http://click.pocoo.org/6/commands/#multi-command-pipelines
