"""
Arguments are similar to options, but more limited
and more restricted and not fully documented.

In addition, arguments are positional
"""

import click


@click.command()
@click.argument('value', default=1)
def cli(value):
    print('value: ', value)

# $ python 04arguments.py
# $ python 04arguments.py f


"""
One of the arguments can be unlimited
"""


@click.command()
@click.argument('src', nargs=-1)
@click.argument('dst', nargs=1)
def cli(src, dst):
    print('from: ', src)
    print('to: ', dst)

# $ python 04arguments.py in1 in2 out


if __name__ == '__main__':
    cli()
