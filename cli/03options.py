"""
Lets explore what you can do with options
"""

import click


"""
### 1
By default, options are assumed to be string.
You can change that by adding a default parameter.
"""


@click.command()
@click.option('--value', default=1)
def cli(value):
    print('value: ', value, type(value))

# $ python 03options.py
# $ python 03options.py --value f


"""
### 2
The type can also be changed using the 'type' keyword argument
"""


@click.command()
@click.option('--value', type=int)
def cli(value):
    print('value: ', value, type(value))

# $ python 03options.py --value 7


"""
### 3
Options can receive multiple values (a fixed number)
"""


@click.command()
@click.option('--value', nargs=2, type=int)
def cli(value):
    print('value: ', value, type(value))

# $ python 03options.py --value 7 5


"""
### 4
Another way to provide a parameter multiple times in using 
the 'multiple' option
"""


@click.command()
@click.option('--value', type=int, multiple=True)
def cli(value):
    print('value: ', value, type(value))

# $ python 03options.py --value 3 --value 6


"""
### 5
Boolean flags are easily made
"""


@click.command()
@click.option('--shout/--no-shout', default=False)
def cli(shout):
    if shout:
        print('Hello world!!!')
    else:
        print('Hello world')

# $ python 03options.py
# $ python 03options.py --shout


"""
### 6
If you do not need the switch-off (for the boolean flag)
"""


@click.command()
@click.option('--debug', is_flag=True)
def cli(debug):
    if debug:
        print('Debug mode enabled')

# $ python 03options.py
# $ python 03options.py --debug


"""
### 7
Choices
"""


@click.command()
@click.option('--ref', type=click.Choice(['hg19', 'hg38']))
def cli(ref):
    print('Using: ', ref)


# $ python 03options.py --ref hg19
# $ python 03options.py --ref hg18


if __name__ == '__main__':
    cli()
