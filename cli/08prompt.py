"""
Click also includes utilities to ask the user for some input
if its has not been provided in the command line.
"""


"""
### 1 
Options have a prompt flag 
"""

import click


@click.command()
@click.option('--name', prompt=True)
def cli(name):
    print('Hi ', name)

# $ python 08prompt.py


"""
###2
The prompt argument can be used to input the prompt text
"""


@click.command()
@click.option('--name', prompt='What is your name?')
def cli(name):
    print('Hi ', name)

# $ python 08prompt.py


"""
### 3
There are also some further customization options
"""


@click.command()
@click.option('--passw', prompt='Access password', hide_input=True, confirmation_prompt=True)
def cli(passw):
    print('You are in')

# $ python 08prompt.py
# $ python 08prompt.py --passw bbg


"""
### 4
Similar utilities can be used along the code, not just as arguments in decorators
"""


@click.command()
def cli():
    name = click.prompt('Please enter your name')
    click.confirm('Hi {}. Do you want to continue?'.format(name), abort=True)
    print('Done')

# $ python 08prompt.py


if __name__ == '__main__':
    cli()
