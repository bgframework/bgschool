"""
A group is a set of commands can be grouped under another command
"""
import functools
import operator

import click



"""
### 1
Default values can be taken from environment variables
"""


@click.command()
@click.argument('value', nargs=-1, type=int)
def sum(value):
    print('Sum: ', functools.reduce(operator.add, value))


@click.command()
@click.argument('value', nargs=-1, type=int)
def mul(value):
    print('Mul: ', functools.reduce(operator.mul, value))


@click.group()
def cli():
    pass


cli.add_command(sum)
cli.add_command(mul)

# $ python 06groups.py
# $ python 06groups.py sum 1 2 3
# $ python 06groups.py mul 1 2 3


"""
### 2
Subcommands can also be added directly in the decorator
"""


@click.group()
def cli():
    pass


@cli.command()
@click.argument('value', nargs=-1, type=int)
def sum(value):
    print('Sum: ', functools.reduce(operator.add, value))


@cli.command()
@click.argument('value', nargs=-1, type=int)
def mul(value):
    print('Mul: ', functools.reduce(operator.mul, value))


# $ python 06groups.py
# $ python 06groups.py sum 1 2 3
# $ python 06groups.py mul 1 2 3


"""
### 3
Each command has its own options and should should be specified before any other subcommand
"""


@click.group()
@click.option('--debug', is_flag=True)
def cli(debug):
    if debug:
        print('Debug mode enabled')


@cli.command()
@click.argument('value', nargs=-1, type=int)
def sum(value):
    print('Sum: ', functools.reduce(operator.add, value))


@cli.command()
@click.argument('value', nargs=-1, type=int)
def mul(value):
    print('Mul: ', functools.reduce(operator.mul, value))


# $ python 06groups.py mul 1 2 3 --debug
# $ python 06groups.py --debug mul 1 2 3


"""
### 4
If needed group commands can also be invoked without sub-commands
"""


@click.group(invoke_without_command=True)
@click.pass_context
def cli(ctx):
    if ctx.invoked_subcommand is None:
        print('Missing operator')
    else:
        ctx.invoked_subcommand


@cli.command()
@click.argument('value', nargs=-1, type=int)
def sum(value):
    print('Sum: ', functools.reduce(operator.add, value))


@cli.command()
@click.argument('value', nargs=-1, type=int)
def mul(value):
    print('Mul: ', functools.reduce(operator.mul, value))


# $ python 06groups.py
# $ python 06groups.py mul 1







if __name__ == '__main__':
    cli()
