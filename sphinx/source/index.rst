.. ProjectDocumentation documentation master file, created by
   sphinx-quickstart on Fri Nov  4 11:58:08 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Documenting a Python project
============================

This guide is intended to provide information on
how to create technical documentation
for a Python project
using `Sphinx`_.

Some considerations taken form
the `developer guide <https://docs.python.org/devguide/documenting.html>`_:

- More documentation is not necessarily better documentation.
- Use a tone respectful of the reader’s intelligence. Don’t talk down to them or waste their time.


Contents:

.. toctree::
   :maxdepth: 2


   intro
   start
   reST-Sphinx
   files
   extras
   summary


Tutorial
--------

Follow the tutorial
for Sphinx.

.. toctree::
   :maxdepth: 2


   tutorial


Further resources:

- Check the `WriteTheDocs documentation guide <http://www.writethedocs.org/guide/>`_



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. _Sphinx: http://www.sphinx-doc.org/
