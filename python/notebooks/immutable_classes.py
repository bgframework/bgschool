# Import modules
import collections
import datetime
import logging

import click
import daiquiri


# Set up daiquiri
daiquiri.setup(level=logging.DEBUG)
logger = daiquiri.getLogger('log')


# Create a nametuple
ImmutableThingTuple = collections.namedtuple("ImmutableThingTuple", "a b c d")


# Create a class
class MutableThing:
    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d


# Create a class with ___slot__
class ImmutableThing:
    __slots__ = ['a', 'b', 'c', 'd']

    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d


# Create a class with ___slot__ and @property
class SuperImmutableThing:
    __slots__ = ['_a', '_b', '_c', '_d']

    def __init__(self, a, b, c, d):
        self._a = a
        self._b = b
        self._c = c
        self._d = d

    @property
    def a(self):
        return self._a


# Classic class
# without_slot = MutableThing(1, 1, 1, 1)
# without_slot.a = 2  # Ok
# without_slot.z = 2  # Ok

# Example of the effect of __slot__
# with_slot = ImmutableThing(1, 1, 1, 1)
# with_slot.a = 2  # Ok
# with_slot.z = 2  # Error

# Example of the effect of __slot__ and @property
# with_slot_and_property = SuperImmutableThing(1, 1, 1, 1)
# with_slot_and_property.a = 2  # Error
# with_slot_and_property.z = 2  # Error


@click.command()
@click.option(
    '-m', '--mode', type=click.Choice(['tuple', 'namedtuple', 'mutable_class', 'immutable_class']), required=True,
    help='Structure to use'
)
@click.option(
    '-i', '--instances', type=click.INT, default=1_000_000,
    help='Number of instances to create'
)
def run(mode, instances):
    """Compare the efficiency of the usage of tuple, namedtuple, classes, and classes with __slot__"""
    logger.info("Working with {:,} instances".format(instances))
    t0 = datetime.datetime.now()
    data = []

    if mode == 'tuple':
        logger.info('Using tuple')
        for n in range(instances):
            data.append((1 + n, 2 + n, 3 + n, 4 + n))

    elif mode == 'namedtuple':
        logger.info('Using namedtuple')
        for n in range(instances):
            data.append(ImmutableThingTuple(1 + n, 2 + n, 3 + n, 4 + n))

    elif mode == 'mutable_class':
        logger.info('Using standard classes')
        for n in range(instances):
            data.append(MutableThing(1 + n, 2 + n, 3 + n, 4 + n))

    elif mode == 'immutable_class':
        logger.info('Using slot based classes')
        for n in range(instances):
            data.append(ImmutableThing(1 + n, 2 + n, 3 + n, 4 + n))

    t1 = datetime.datetime.now()
    logger.info("Finished, waiting... done in {:,} s".format((t1 - t0).total_seconds()))


if __name__ == '__main__':
    run()
