

Classes
=======

**Classes** are code templates for object creation
and **objects** are instances of classes.
Classes are the base of many Object Oriented languages [#oop]_.

Python is on a pure Object Oriented language. In fact,
it is multi-paradigm, however misses some features of other functional programming languages
(see https://stackoverflow.com/questions/1017621/why-isnt-python-very-good-for-functional-programming,
https://docs.python.org/3.5/howto/functional.html).

.. topic:: Paradigms

   Some programming paradigms are:

   - *Imperative*: list of instructions (e.g. Fortran or C)
   - *Declarative*: describe the problem to be solved (e.g. SQL)
   - *Functional*: the problem is decomposed into a list of functions (e.g. Haskell or Standard ML)
   - *Object-oriented*: program manipulate objects (with an internal stated modified by methods)


Python classes are a mixture of the class mechanisms found in C++ and Modula-3:

- multiple base classes are allowed
- a derived class can override any methods of its base class(es)
- a method can call the method of a base class with the same name
- are created at runtime
- can be modified further after creation
- members are *public*
- member functions are *virtual*
- methods are declared with an explicit first argument representing the object
- built-in types can be used as base classes
- built-in operators with special syntax can be redefined
- aliasing is possible: multiple names (in multiple scopes) can be bound to the same object
  for mutable objects
- when a class definition is entered, a new namespace is created, and used as the local scope


.. warning::

   These examples are probably far from the best way of doing things.
   They are just examples trying to showcase particular features.

Usage
-----

Use classes when the *state* (or similar information) is needed;
or when you need *encapsulation* (procedure and data are stored together).

Do not use classes to group related methods. To do that, use a module.

If methods are only going to be called once, you do not need a class.

In scientific computing, classes are usually not needed
and in most cases not a good idea.
In a typical case in which you need to call several functions consecutively
do not use a class and call all methods from the constructor.
For that, use a function that calls the others.
In such a case, a class can be useful if you are passing a lot of parameters
that are the same to all the functions.

---

.. tip::

   - Do not use classes "by default"
   - Consider using classes when you are passing the same parameters to several methods
   - Try to make clear who is responsible/owner of each object,
     not only creation but also *destruction*
     (the garbage collector does a great job, this is a design tip).
     Take care with cyclic references.

----

Classes and parallel computing
******************************

Classes and parallel computing are not close friends.
However, it is difficult to give hints on how to play
with them to avoid performance downfalls.
Ideally, do not use classes on the code where parallelization is performed.

In a program that runs in parallel, you can think of a class as a dictionary.
The whole thing will be serialized to each process.
Thus, do not carry useless items to each process.

Definition and instantiation
----------------------------

.. https://www.youtube.com/watch?v=anwy2MPT5RE
.. https://www.youtube.com/watch?v=8I3zCQzZx68
.. https://www.youtube.com/watch?v=4vuW6tQ0218

Classes are defined with the keyword **class**.
The naming convention is to use *CamelCase*.
E.g. ::

   >>> class MontyPythonSketch:
   ...     pass
   ...
   >>> print(type(MontyPythonSketch))
   <class 'type'>



Class objects support two kind of operations:
    - *attribute references*: ``obj.name``
    - *instantiation*: creates an object::

         >>> spam = MontyPythonSketch()
         >>> print(type(spam))
         <class '__main__.MontyPythonSketch'>


.. https://www.youtube.com/watch?v=8I3zCQzZx68
.. https://www.youtube.com/watch?v=4vuW6tQ0218

New instances are different objects in memory while
the class is only one piece of code::

	>>> Sketch_1 = MontyPythonSketch
	>>> hex(id(Sketch_1))
	'0x55fd23d4abc8'
	>>>
	>>> Sketch_2 = MontyPythonSketch
	>>> hex(id(Sketch_2))
	'0x55fd23d4abc8'
	>>>
	>>> killer_joke = MontyPythonSketch()
	>>> hex(id(killer_joke))
	'0x7f0d8698c358'
	>>>
	>>> dead_parrot = MontyPythonSketch()
	>>> hex(id(dead_parrot))
	'0x7f0d8698c320'

.. _python class members:

Members
-------

Typically, we can find 3 types of members in a class.

.. contents::
   :local:


E.g.::

   class Knight:
       castle = 'Camelot'  # Class data attribute

    def set_name(self, value):  # method
        self.name = value  # instance data attributef

:ref:`Properties <python property>` lay in between methods
and data attributes. They are read and write as data,
but under the hood, it is done as methods.

.. https://en.wikipedia.org/wiki/Monty_Python_and_the_Holy_Grail

.. https://www.youtube.com/watch?v=JHFXG3r_0B8

Methods
*******

Methods are functions intended to change the state or behaviour of the instance.
The first parameter they receive is a class instance.
This parameter is normally named as ``self`` [#self]_.
However, when an instance calls them, this parameter is already set::

  >>> arthur = Knight()
  >>> arthur.set_name('King Arthur')  # Note that the string is the value parameter and 'self' is somehow hidden
  >>> arthur.name
  'King Arthur'

As mentioned, methods require and instance, so we cannot call a method from the class...

::

    >>> Knight.set_name('King Arthur')
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: set_name() missing 1 required positional argument: 'value'

.. index:: peculiar class; passing instance

... unless providing the instance

::

    >>> lancelot = Knight()
    >>> Knight.set_name(lancelot, 'Sir Lancelot the Brave')  # the first parameter of the method is a class instance
    >>> lancelot.name
    'Sir Lancelot the Brave'

.. https://www.youtube.com/watch?v=GJoM7V54T-c


Instance method objects have two interesting attributes:

- :attr:`__self__`: instance object with the method m()
- :attr:`__func__`: function object corresponding the method


Instance data attributes
*************************


Instance data attributes are data members associated with an instance, so they cannot be found in the class itself.

::

    >>> print(Knight.name)
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AttributeError: type object 'Knight' has no attribute 'name'

Their goal is to hold the state of the object.
They live within the instance and can be accessed via attribute references (``intance_name.attribute``).
In Python these attributes are added dynamically::

    >>> galahad = Knight()
    >>> galahad.name
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AttributeError: 'Knight' object has no attribute 'name'


Thus, they do not exist in the instance until they are set::

    >>> galahad.set_name('Sir Galahad the Pure')  # use the instance to modify the status
    >>> galahad.name  # can be called
    'Sir Galahad the Pure'


Class data attributes
*********************

Class data attributes belong to the class and are shared by the instances.
Thus they can be addressed from the class or from an instance::

    >>> print(Knight.castle, galahad.castle, sep=', ')
    Camelot, Camelot


On the other hand, any instance can have a different value if explicitly set::

    >>> galahad.castle = 'Anthrax'  # changing the value of the instance lets the class unchanged
    >>> print(Knight.castle, galahad.castle, sep=', ')
    Camelot, Anthrax

.. https://www.youtube.com/watch?v=e0A5vzGMQr8

.. warning::

  The above example is a missuse of class attributes. Please, do not do that.
  Class data attributes are rarely non-constant.


Changing the class value alters the value of all the instances that have not been altered::

    >>> ector = Knight()
    >>> Knight.castle = 'Castle of Aaaaargh'
    >>> print(Knight.castle, galahad.castle, ector.castle, sep=', ')
    Castle of Aaaaargh, Anthrax, Castle of Aaaaargh

.. https://www.youtube.com/watch?v=ZlIz0q8aWpA


Initialization
--------------

Initialization of classes is done with the ``__init__`` method.
It is typically use to set the initial state of the class instance

::

    >>> class Table:
    ...     def __init__(self, chairs):
    ...         self.free_chairs = chairs  # inital state from external parameters
    ...         self.knights = []  # same initial state for this variable in all instances
    ...
    ...     def add_knight(self, name):
    ...         if self.free_chairs > 0:
    ...             self.knights.append(name)
    ...             self.free_chairs -= 1
    ...         else:
    ...             print('No more chairs')
    ...
    >>> round_table = Table(12)
    >>> round_table.free_chairs
    12
    >>> round_table.add_knight('Sir Robin the Not-Quite-So-Brave-As-Sir-Lancelot')
    >>> round_table.free_chairs
    11

.. https://www.youtube.com/watch?v=c4SJ0xR2_bQ

If it is not explicitly written, a default empty method is used.


Members accessibility
---------------------

On many programming languages with classes you find 3 keywords that indicate the level of accessibility for the class members:

- *private*: restricts the access to the class itself
- *protected*: allows the class itself and all its subclasses to access the member
- *public*: any code can access the member by its name

In Python, all members are **public**.
Protected and private members are identified by convention:
names prefixed with an underscore
(e.g. ``_spam``) should be treated as non-public part of the API.
Thus, use ``_`` before the member name to mark it as non public (similar to protected)
and use ``__`` to mark it as private.

.. index:: peculiar class; name mangling

Since private members can be used to avoid clashes with names in subclasses,
*name mangling* is supported. This mechanism forces that names with
at least two leading underscores and at most one trailing underscore
are textually replaced adding the classname in front (with an underscore):
``__member``   -> ``_Class_member``.

Lets see it with an example

::

    >>> class Knight:
    ...     def __init__(self, name, answer):
    ...         self.name = name
    ...         self._quest = 'To seek the Holy Grail'
    ...         self.__3rd_question_answer = answer
    ...     def get_answer(self):
    ...         return self.__3rd_question_answer
    ...
    >>> robin = Knight('Sir Robin of Camelot', 'Kalhu')


.. https://www.youtube.com/watch?v=Wpx6XnankZ8

**Public** member are always accessible::

    >>> print('What is your name?')
    What is your name?
    >>> robin.name
    'Sir Robin of Camelot'


**Protected** members are accessible::

    >>> print('What is your quest?')
    What is your quest?
    >>> robin._quest
    'To seek the Holy Grail'

.. note::

   Although protected member should not be used as public,
   there is no limitation to do so.


.. _private members:

Name mangling makes **private members** are not directly accessible from outside the class::

    >>> print('What is the capital of Assyria?')
    What is the capital of Assyria?
    >>> robin.__3rd_question_answer
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AttributeError: 'Knight' object has no attribute '__3rd_question_answer'


But they are accessible from the inside the class::

    >>> print('What is the capital of Assyria?')
    What is the capital of Assyria?
    >>> robin.get_answer()
    'Kalhu'


Remember that *name mangling* is just a textual replacement,
so you can still access private members from outside::

    >>> print('What is the capital of Assyria?')
    What is the capital of Assyria?
    >>> robin._Knight__3rd_question_answer
    'Kalhu'


There have restrictions for :func:`exec`, :func:`eval`, :func:`getattr`, :func:`setattr`, :func:`delattr`
and :attr:`__dict__`.


Find another example in `<https://docs.python.org/3/tutorial/classes.html#private-variables>`_.


.. warning::

   *Name mangling* does not affect **special methods** (``__method__``)::

        >>> class BridgeKeeper:
        ...     def __ask__(self):
        ...             return 'Stop! Who would cross the Bridge of Death must answer me these questions three, ere the other side he see.'
        ...
        >>> bridgekeeper = BridgeKeeper()
        >>> bridgekeeper.__ask__()
        'Stop! Who would cross the Bridge of Death must answer me these questions three, ere the other side he see.'


Static methods
--------------

Static methods within a class are similar to class data attributes but applied to methods.
They are class methods that are not related with the instance but with the class itself,
so all instances behave equally.
Thus, they do not require any instance and they cannot modify any instance's status.

To declare a method as static, use the ``@staticmethod`` decorator.

E.g.::

    >>> class RoundTableKnight:
    ...     @staticmethod
    ...     def what_are_you_looking_for():  # the instance is not passed to an static method
    ...         return 'The Holy Grail'
    ...
    >>> sir_gawain = RoundTableKnight()


Static method can be called from the instances...::

    >>> sir_gawain.what_are_you_looking_for()
    'The Holy Grail'

... and from the class::


    >>> RoundTableKnight.what_are_you_looking_for()
    'The Holy Grail'


Inheritance
-----------

Inheritance occurs when a class is based on another.
In Python any class inherits from `object <https://docs.python.org/3/library/functions.html?highlight=object#object>`_.

E.g.::

    >>> class Prophet:
    ...     def breath(self):
    ...         print('breathing')
    ...     def salve(self):
    ...         print('Follow me to salvation')
    ...     def ramble(self):
    ...         print('bla bla bla')
    ...
    >>> class MontyPythonProphet(Prophet):
    ...     def salve(self):
    ...         print("Look, you've got it all wrong! You don't NEED to follow ME, You don't NEED to follow ANYBODY! You've got to think for your selves! You're ALL individuals!")
    ...     def ramble(self):
    ...         super().ramble()
    ...         print('... something funny goes on ...')
    ...         super().ramble()
    ...
    >>> brian = MontyPythonProphet()


.. https://www.youtube.com/watch?v=Yu_PWm2zgbg

Derived class methods are inherited::

    >>> brian.breath()
    breathing


In addition, methods can be overridden in derived classes.
Any instance call its methods rather than its parent's (equivalent to declare methods as ``virtual`` in C++)::

    >>> brian.salve()
    Look, you've got it all wrong! You don't NEED to follow ME, You don't NEED to follow ANYBODY! You've got to think for your selves! You're ALL individuals!


However, you can explicitly call the base class method::

    >>> Prophet.salve(brian)
    Follow me to salvation


If you want to extend a class method rather than overriding it,
you can call explicitly the base class method (Base.method(self, ..)) or use :meth:`super` `<https://docs.python.org/3.5/library/functions.html#super>`_::

    >>> brian.ramble()
    bla bla bla
    ... something funny goes on ...
    bla bla bla


In Python there are 2 built-in functions related to inheritance:
    - :func:`isinstance`
    - :func:`issubclass`


Multiple
********

Python supports multiple inheritance.
Multiple inheritance occurs when classes inherit from more than one class.

E.g.::

    >>> class Character:
    ...     def play(self):
    ...         print('character playing')
    ...     def talk(self):
    ...         print('character talking')
    ...
    >>> class Knight:
    ...     def fight(self):
    ...         print("'Tis but a scratch!\nCome on, you pansy!")
    ...     def talk(self):
    ...         print('knigth talking')
    ...
    >>> class MontyPythonKnight(Character, Knight):
    ...     pass
    ...
    >>> black_knight = MontyPythonKnight()


.. https://www.youtube.com/watch?v=dhRUe-gz690


As in single inheritance derived class inherits methods from its parents::

    >>> black_knight.play()
    character playing
    >>> black_knight.fight()
    'Tis but a scratch!
    Come on, you pansy!


But, what happen if there a conflicts with methods?
In multiple inheritance, method resolution is done using a linearization algorithm :abbr:`MRO (Method Resolution Order)`,
that briefly makes a depth-first left to right search [#mro]_::


	>>> black_knight.talk()
    character talking


If it cannot find a consistent way for resolution, an exception is raised.

E.g.::

    >>> class CharKnight(Character, Knight): pass
    ...
    >>> class KnightChar(Knight, Character): pass
    ...
    >>> class Mess(CharKnight, KnightChar): pass
    ...
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: Cannot create a consistent method resolution
    order (MRO) for bases Character, Knight


For more information:
`<https://stackoverflow.com/questions/3277367/how-does-pythons-super-work-with-multiple-inheritance http://python-history.blogspot.com.es/2010/06/method-resolution-order.html>`_,
`<https://docs.python.org/3/tutorial/classes.html#multiple-inheritance>`_,
`<http://python-history.blogspot.com.es/2010/06/method-resolution-order.html>`_



Extras
------

Some special attributes are:

- :attr:`__doc__` refers to the docstring
- :attr:`__class__` refers to the type

Other built-in decorators that can be used with class methods are ``classmethod``

.. toctree::
   :maxdepth: 1
   :caption: Special classes

   classes/empty
   classes/special
   classes/iterator
   classes/context
   classes/property
   classes/abc
   classes/exceptions
   classes/new
   classes/meta
   classes/builtin
   classes/slots





.. [#oop] Object Oriented Programming is basically the design of a program as a set of objects that interact with one another.
   OOP was developed to increase maintanability, scalability and reusability of source code.
   However, OOP has been criticised different reasons:

   - Not being able to make modular an reusable code (one of its goals) specially when it comes to class extension and modification
   - Emphasising on design and modelling at the cost of computation
   - Adding too much ovehead:

      "The problem with object-oriented languages is they've got all this implicit environment that they carry around with them. You wanted a banana but what you got was a gorilla holding the banana and the entire jungle."

      -- Joe Armstrong

   - Not boosting productivity

.. [#self] ``self`` is used just by convention, it has no special meaning, but USE IT

.. [#mro] In fact, it is slightly more complex than that;
   the method resolution order changes dynamically to support cooperative calls to :func:`super`.
   This approach is known in some other multiple-inheritance languages as call-next-method.
   This dynamic ordering is needed to keep the common base classes from being accessed
   more than once. E.g. Base1 and Base2 inherit from Object so Object can be reached
   from multiple paths from the bottom class.

