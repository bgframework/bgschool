
.. index:: function; generator


.. _generators:

Generators
==========

Generators are powerful tools for creating iterators.
They are like regular functions but with a :ref:`yield <yield>` statement.

When :func:`next` is called on it, the generator resumes where it left off.

Anything that can be done with generators can also be done with :ref:`class-based iterators <iterator class>`.
What makes generators so compact is that
the :meth:`__iter__` and :meth:`__next__` methods are created automatically,
and they also raise a :exc:`StopIteration` automatically on termination.

Following the :ref:`class-based iterator example <iterator class example>`::

    >>> def triplet(a, b, c):
    ...     seq = [a,b,c]
    ...     for n in seq:
    ...             yield n
    ...
    >>> for nucleotide in triplet('A', 'C', 'T'):
    ...     print(nucleotide, end=', ')
    ...
    A, C, T,

Another key feature is that the local variables and execution state are automatically saved between calls::

    >>> it = iter(triplet('A', 'C', 'T'))
    >>> next(it)
    'A'
    >>> next(it)
    'C'
    >>> next(it)
    'T'


Generators can also receive values with the method :meth:`generator.send`.
Find more details in `<https://docs.python.org/3/howto/functional.html#passing-values-into-a-generator>`_.


Generator expressions
---------------------

Some simple generators can be coded succinctly as expressions
using a syntax similar to list comprehensions but with parentheses instead of brackets.
These expressions are designed for situations where the generator is used right away by an enclosing function.
Generator expressions are more compact but less versatile than full generator definitions
and tend to be more memory friendly than equivalent list comprehensions.

.. code-block:: python

    >>> sum(i*j for i in range(10) if i != 5 for j in range(20) if j%2==0)
    3600

