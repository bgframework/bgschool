
Regular expressions
===================

Regular expressions are available through the
:mod:`re` module.

Meta characters
---------------

The special characters (meta characters) used in regexes are::

   . ^ * + ? { } [ ] \ | ( )

To match any of those, their special meaning can be removed by
escaping them. E.g. to match ``[`` use ``\[``.

Matching characters
-------------------

[ ]
    Define a set of characters you want to match (character class).
    Inside, the ``-`` symbol is used to indicate ranges: [a-z].
    Additionally, the meta characters are not active
    inside character classes.

    E.g.:

    [abc]
        match ``'a'`` or ``'b'`` or ``'c'``

    [a-c]
        match ``'a'`` or ``'b'`` or ``'c'``

    [at$]
        match ``'a'`` or ``'t'`` or ``'$'``

[^ ]
    Define a **complementary** set. It means it will match any character but
    the ones indicated.

    E.g.:

    [^a]
        match any character except ``'a'``

    [^^]
        match any character except ``'^'``

.. note::

    **Character sequences that are also accepted in sets**
    (`find all <https://docs.python.org/3/library/re.html#re-syntax>`_):

    ``\d``
        any decimal digit (``[0-9]``)

    ``\D``
        any non-digit character (``[^0-9]``)

    ``\s``
        any whitespace character(``[ \t\n\r\f\v]``)

    ``\S``
        any non-whitespace character (``[^ \t\n\r\f\v]``)

    ``\w``
        any alphanumeric character (``[a-zA-Z0-9_]``)

    ``\W``
        any non-alphanumeric character (``[^a-zA-Z0-9_]``)

    In general, the Unicode versions match any character
    that’s in the appropriate category in the Unicode database [#unicode]_.

``.``
    matches anything except a newline character [#dotall]_

Groups
------

Groups can be used to match
different components of interest
within a regex.

Groups are indicated with ``( )``.
They are numbered starting with 0.
0 group is always present and corresponds to
the whole regex. The rest of groups are
numbered from left to right.

See examples at: `<https://docs.python.org/3/howto/regex.html#grouping>`_.

Additionally, backreferences are allowed in patterns.
The syntax is ``\number`` where *number* is the number of
the group.
Then the match will succeed if the exact content of the
referred group can be found in the current positions.

E.g.
    Finding a word copied twice (consecutive)

    .. code-block:: python

       >>> p = re.compile(r'\b\w+') # regex for word
       >>> p = re.compile(r'(\b\w+)\s+\1') # regex for repeated word
       # the content of group 1 is expected to happen after some space
       >>> p.search('Paris in the the spring').group()
       'the the'


``(?:...)``
    non-capturing group

``(?P<name>...)``
    named group (can be referred by number of *name*)

    To make a reference to a name-group inside the regex
    use ``(?P=name)``

    E.g.
        Doubled words (regex for repeated word)

        .. code-block:: python

            >>> p = re.compile(r'(?P<word>\b\w+)\s+(?P=word)')


Zero-with assertions
--------------------

``|``
    *or* operator between regexes. Has low precedence:
    ``monty|python`` will match ``monty`` or ``python``
    (not ``mont`` and ``'y'`` or ``'p'`` or
    ``'y'`` or ``'p'`` and ``ython``)

``^``
    start of string [#multilinestart]_

``$``
    end of string [#multilineend]_

``\A``
    start of string

``\Z``
    end of string

``\b``
    word boundary
    (indicated by whitespace or a non-alphanumeric,
    non-underscore Unicode [#unicode]_ character).

    E.g.
        ``r'\bfoo\b'`` matches ``'foo'``, ``'foo.'``, ``'(foo)'``, ``'bar foo baz'``
        but not ``'foobar'`` or ``'foo3``'

    Inside a character class represents the backspace character.

    .. warning::

        In a Python string ``\b`` is converted to the backspace character
        so use raw string in the regexes if the word boundary is desired.

``\B``
    opposite of ``\b`` (match if the position is not a word boundary)


Lookahead assertions
--------------------

Lookahead assertions evaluate a regex at current location
but the matching engine does not adavance at all.
If they succeed the rest
of the pattern is tried right where the assertion started.

``(?=...)``
    positive lookahead assertion

``(?!...)``
    negative lookahead assertion

Find an example here: `<https://docs.python.org/3/howto/regex.html#lookahead-assertions>`_.

Repeating
---------

The following meta characters alter the behaviour of
preceding pattern to match them a certain number of times:

``*``
    0 or more times

``+``
    1 or more times

``?``
    0 or 1 times

``{m, n}``
    at least *m* repetitions, at most *n*. Either *m* or *n* can be omitted

All this qualifiers are *greedy*: they will attempt to match as many repetitions as possible.
E.g. the regex ``<.*>`` matched against  ``'<a> b <c>'`` will match the entire string, not ``<a>``.

See `an example of how the engine works
<https://docs.python.org/3/howto/regex.html#repeating-things>`_.

This *greedy* behaviour can be modified to try to match as few characters as possible.
To do so, the qualifier ``?`` needs to be added after any of the others.
E.g. the regex ``<.*?>`` matched against  ``'<a> b <c>'`` will match only ``<a>``.

Ussing regexes
--------------

Regexes are compiled into pattern objects using the function :func:`re.compile`.
This function received the regex as a string.

.. code-block:: python

    pattern = re.compile('ab*')

Backslash issue
***************

As backslashes are used to escape character in strings, you need to
escape them if you want a backslash in your regex.

E.g.

    Assume you want to match the string ``\section``.

    Then, as ``\`` is a meta character of the regex, it should be escaped.
    Then the regex is ``\\section``.

    Now, which string should be provided to the :func:`re.compile` function?

    As ``\`` is the character used to scape characters in string, each ``\``
    should be escaped: ``'\\\\section'`` (or alternative use raw strings:
    ``r'\\section'``).

Matching
********

The main functions used are:

:func:`re.match`
    determine if the regex matches at the begging (return ``None`` if not)

:func:`re.search`
    scan through the string until the first match (return ``None`` if it does not match anywhere)

:func:`re.findall`
    find all non-overlapping matches of *pattern* (return as :obj:`list`)

:func:`re.finditer`
    find all non-overlapping matches of *pattern* (return as iterator)

Demo at `<https://hg.python.org/cpython/file/3.5/Tools/demo/redemo.py>`_.

The match object
^^^^^^^^^^^^^^^^

The `match object <https://docs.python.org/3/library/re.html#match-objects>`_
contains information about the match.

The most important methods are:

:meth:`match.group`
    return the string matched

:meth:`match.span`
    return the start and end position of the match
    (also accessible with :meth:`match.start` and
    :meth:`match.end`)


String modification
-------------------

Regular expressions can also be used to perform modifications in strings.
Compiled patter have the following methods:

:meth:`split`
    split the string when the pattern matches (groups are returned)

:meth:`sub`
    replace the leftmost non-overlapping occurrences

:meth:`subn`
    same as :meth:`sub` but also return the number or replacements

.. warning::

    For simple string operations, not using regular expressions
    is usually faster. Take a look at string methods.


.. [#unicode] The behaviour can be modified to be more restrictive (only ASCII characters)
    by providing the ASCII flag.

.. [#dotall] The behaviour can be modified match also new line characters
    by providing the DOTALL flag.

.. [#multilinestart] he behaviour can be modified match also after each
    new line character
    by providing the MULTILINE flag.

.. [#multilineend] he behaviour can be modified match also before each
    new line character
    by providing the MULTILINE flag.
