
.. _functions:

Functions
=========

Functions format::

    def function_name(arguments):
        body

The keyword :ref:`def <def>` introduces a function definition.
It must be followed by the function name and the parenthesized list of formal parameters.
The statements that form the body of the function start at the next line, and must be indented.

The execution of a function introduces a new symbol table used for the local variables of the function.
More precisely, all variable assignments in a function store the value in the local symbol table;
whereas variable references first look in the local symbol table,
then in the local symbol tables of enclosing functions,
then in the global symbol table,
and finally in the table of built-in names.
Thus, global variables cannot be directly assigned a value within a function
(unless named in a :ref:`global <global>` statement),
although they may be referenced.

The actual parameters (arguments) to a function call are introduced in the local symbol table of the called function when it is called;
thus, arguments are passed using *call by value*
(where the value is always an object reference, not the value of the object) [#arguments]_.

A function definition introduces the function name in the current symbol table.
The value of the function name has a type that is recognized by the interpreter as a user-defined function.
This value can be assigned to another name which can then also be used as a function.



Arguments
---------

Default argument values
***********************

A default value can be specified for one or more arguments::

    def f(arg1, arg2='default value'):
        ...


.. index:: peculiar functions; default values

.. important::

    The default values are evaluated at the point of function definition in the defining scope.

    This script::

        i=5
        def f(arg=i):
            print(i)
        i=6
        f()

    prints: ``5``


.. warning::

    The default value is evaluated only once. This makes a difference when the default is a mutable object.

    .. code-block:: python

        def f(value, l=[]):
            l.append(value)
            return l

        print(f(1))  # [1]
        print(f(2))  # [1, 2]
        print(f(3))  # [1, 2, 3]

    If this behaviour is not desired:

    .. code-block:: python

        def f(value, l=None):
            if l is None:
                l=[]
            l.append(value)
            return l


Keyword arguments
*****************

In functions arguments are positional:
first argument in the call is first argument in the function argument list::

    def f(arg1, arg2, arg3):
        pass

    f(1, 2, 3)  # assign arg1->1, arg2->2, arg3->3

However, the others can be altered using the keyword or the arguments::

    def f(arg1, arg2, arg3):
        pass

    # 1 positional argument and 2 keywords
    f(1, arg3=3, arg2=2)  # assign arg1->1, arg2->2, arg3->3


In a function call, keyword arguments must follow positional arguments.
All the keyword arguments passed must match one of the arguments accepted by the function,
and their order is not important.
This also includes non-optional arguments.

Arbitrary argument list
***********************

The least frequently used option is to specify that a function can be called with an arbitrary number of arguments.
These arguments will be wrapped up in a tuple
Before the variable number of arguments, zero or more normal arguments may occur.

These arguments will be last in the list of formal parameters.
Behind only ``keyword`` arguments might appear.


All together
************


In a function definiton like this::

    def f(required, *arguments, **keywords):
        ...

``required``
    is a formal parameter
``*arguments``
    is a tuple with all positional arguments beyond the formal parameter list
``**keywords``
    is a dictionary containing all keyword arguments except for those corresponding to a formal parameter


.. index:: peculiar functions; unpack args

Unpacking argument lists
************************

When the arguments are already in a list or tuple
but need to be unpacked for a function call requiring separate positional arguments.

If they are not available separately, write the function call with the ``*``-operator
to unpack the arguments out of a list or tuple::

    >>> args = [3, 6]
    >>> list(range(*args))  # equivalent to list(range(3, 6))
    [3, 4, 5]


In the same way, dictionaries can deliver keyword arguments with the ``**``-operator::

    >>> def f(arg1, arg2):
        print('arg1: ', arg1, 'arg2: ', arg2 )
    >>> d = {' arg2' : 22, 'arg1': 7}
    >>> f(**args)  # equivalent to list(range(3, 6))
    arg1: 7 arg2: 22


.. note::

   The *starred* expressions (``*``) does not need to appear only in function arguments.
   They can also appear on the left part on an assiments to capture all remaining items
   in a list. E.g.::

        >>> first, *rest = (1,2,3,4)
        >>> first
        1
        >>> rest
        [2, 3, 4]

   Whats more, you can also capture intermediate values. E.g.::

        >>> first, *rest, last = (1,2,3,4)
        >>> first
        1
        >>> last
        4
        >>> rest
        [2, 3]

   More detail in https://www.python.org/dev/peps/pep-3132/



Return
------

Even functions without a :ref:`return statement <return>` do return a value,
albeit a rather boring one: :obj:`None`.


.. index:: function; lambda expressions

Lambda expressions
------------------

Small anonymous functions can be created with the :ref:`lambda <lambda>` keyword.

- They are syntactically restricted to a single expression.
- They can reference variables from the containing scope.

Example::

    >>> square = lambda x: x**2
    >>> square(2)
    4


.. index:: peculiar functions; annotations

Annotations
-----------

:ref:`Function annotations <function>` are completely optional metadata information
about the types used by user-defined functions.

Annotations are stored in the :attr:`__annotations__` attribute of the function as a dictionary
and have no effect on any other part of the function.

Example::

    >>> def f(arg: str = 'nothing') -> int:
            return (len(arg))

.. index:: function; operators

Operators as functions
----------------------

In the :mod:`operator` module a set of functions corresponding to Python’s operators
can be found, e.g. :func:`operator.ne` (same as ``!=``).

Higher order
------------

Higher-order functions take one or more functions as input and return a new function.
The :mod:`functools` module contains some of those.

Amongst all the tools in this module, the most useful is :func:`functools.partial`
which allows creating a *partial function application* (a function with
some of the arguments already fixed). E.g. from a function ``f(x, y, z)``
a function ``g(y, z)`` equivalent to ``f(1, y, z)`` is wanted.


.. [#arguments] *Call by object reference* would be a better description,
    since if a mutable object is passed,
    the caller will see any changes the callee makes to it
    (e.g. items inserted into a list).

