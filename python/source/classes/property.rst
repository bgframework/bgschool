
.. _python property:

Property
========

A **property** is a special :ref:`class member <python class members>`
that lays in between methods and data attributes.
They provide a data-like syntax for reading/writing (``instance.member``),
but "under the hood" getter and setter methods are called.

Properties can be useful for doing data validation,
or making read-only fields.

Imagine you want to have a value, that can be accessed from the outside,
*only for reading*. You can make it protected or private, and use a method
to return its value.
E.g.::

	>>> class C:
	...     def __init__(self, value):
	...         self._x = value
	...
	...     def getx(self):
	...         return self._x
	...
	>>> c = C(7)
	>>> c.getx()
	7


Using the :class:`property` class, ``x`` can be accessed as a data member,
calling that function "under the hood"::

	>>> class C:
	...     def __init__(self, value):
	...         self._x = value
	...
	...     def getx(self):
	...         return self._x
	...
	...     x = property(getx)
	...
	>>> c = C(7)
	>>> c.x  # getx is called
	7

:class:`property` also accepts arguments for setters and deleters, not only getters.


:class:`property` can also be used as a decorator, that acts as a getter,
and then a getter and deleter and be added easily::


	>>> class C:
	...     def __init__(self, initial_value=None):
	...         self._x = initial_value
	...
	...     @property
	...     def x(self):  # getter
	...         return self._x
	...
	...     @x.setter
	...     def x(self, value):  # setter
	...         self._x = x
	...
	>>> c = C(7)
	>>> c.x  # getter called
	7
	>>> c = 8  # setter called
	>>> c.x  # getter called
	8

Further reading in the Python docs: https://docs.python.org/3/library/functions.html#property
