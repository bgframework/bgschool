
.. _python slots:

__slots__
=========

`__slots__ <https://docs.python.org/3/reference/datamodel.html#slots>`_
allows to explicitly state which instance attributes you expect your object instances to have.

It changes instances from a dict-like object to a
named-tuple-like object.

Using ``__slots__`` you indicate the attributes
any instance could have::

	>>> class Point:
	...     __slots__ = ['x', 'y']
	...
	>>> p = Point()
	>>> p.x= 2


However, similar to :ref:`namedtuples <python namedtuple>`
new attributes are not accepted::

	>>> p.z = 5
	Traceback (most recent call last):
	  File "<stdin>", line 1, in <module>
	AttributeError: 'Point' object has no attribute 'z'

The advantage of using ``__slots__`` is that
the instances have faster attribute access
and uses less memory.

More info: https://stackoverflow.com/questions/472000/usage-of-slots
