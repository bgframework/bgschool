

The ``__new__`` method
======================

.. |new| replace:: :meth:`__new__`
.. |init| replace:: :meth:`__init__`


Instances of classes are created using the |init| method. However,
the |new| method also exits.

In short, |new| handles object creation and |init| object
initialization.

|new| is the first step of instance creation and is responsible of
*returning* a new instance of the class. It is a static method that receives
the class as argument (not the instance). E.g.::

    class A:

        def __new__(cls):
            print('Calling new')

        def __init__(self, value=5):
            self.value = value

::

    >>> a = A()
    Calling new


The |new| method must return a new object instance. Otherwise,
the object is of ``NoneType`` and the |init| is never called::

    >>> a = A()
    >>> a.value
    ... # lines skipped
    AttributeError: 'NoneType' object has no attribute 'value
    >>> type(a)
    NoneType

On the other hand, |init| cannot return anything.

An example of how to return a class using :meth:`super`::

    class A:

        def __new__(cls):
            print('Calling new')
            return super().__new__(cls)

        def __init__(self, value=5):
            self.value = value

However, you can use this feature to play any dirty trick you want.

.. note:: |new| intended mainly to allow subclasses of immutable types to
   customize instance creation. In general, you should not override the |new|
   method.


More details in https://docs.python.org/3/reference/datamodel.html#object.__new__
