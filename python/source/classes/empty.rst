
.. _python empty class:

Empty classes
=============

`Empty classes <https://docs.python.org/3/tutorial/classes.html#odds-and-ends>`_ can be used to bind together named data items (as C structs)::

    >>> class Front: pass
    ...
    >>> front = Front()
    >>> front.name = "People's Front of Judea"
    >>> front.members = ['Brian', 'Reg', 'Stan/Loretta']

.. https://www.youtube.com/watch?v=WboggjN_G-4, https://www.youtube.com/watch?v=ZXVN7QJ8m88

However, for such cases consider also using a
`NamedTuple <https://docs.python.org/3/library/collections.html#collections.namedtuple>`_.
