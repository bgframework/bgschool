
Abstract classes and interfaces
===============================

An **interface** is a "contract". It defines how a class should look like
by defining the signature [#signature]_ of the methods.

In Python, they is no way to create interfaces as in other languages like C++ or Java.
However, you can create something similar by annotating a class and not
implementing the methods::

    >>> class IVehicle:
    ...
    ...     def move(self, distance: float) -> float:
    ...         raise NotImplementedError
    ...


**Abstract classes** are similar but they have the implementation of some methods.
It is a way to say: "all classes (that inherit from this) look like this
and have these methods in common so fill the blanks"::


    >>> class ABCVehicle:
    ...
    ...     def move(self):
    ...             raise NotImplementedError
    ...
    ...     def stop(self):
    ...             self.speed = 0
    ...


As from `PEP 3119 <https://www.python.org/dev/peps/pep-3119/>`_ the :mod:`abc` module
has been introduced to enable the creation of abstract classes easily.
However, for simple cases a similar result can be obtained by raising
:exc:`NotImplementedError` from the methods [#notimplemented]_.


.. [#signature] The signature essentially consists of the method name
   and the in and out parameters.

.. [#notimplemented] Special methods (e.g. ``__add__``)
   should **return** ``NotImplemented``.
   Read more in https://stackoverflow.com/questions/878943/why-return-notimplemented-instead-of-raising-notimplementederror
