
Built-in classes
================

Some useful built-in classes are:

.. contents::
   :local:


SimpleNamespace
---------------

With :class:`~types.SimpleNamespace` you can
create a class from keyword arguments,
where keys are later addressable
as instance attributes::

	>>> from types import SimpleNamespace
	>>> sn = SimpleNamespace(x=7,y=5)
	>>> sn.x
	7


Beside this special initialization,
it is similar to an :ref:`emtpy class <python empty class`.

.. _python namedtuple:

Namedtuple
----------

:class:`~collections.namedtuple` is a class
that behaves like a tuple, but its members are addressable
like any attribute.

New classes are defined with a name and names
of the fields it will contain::

	>>> Point = namedtuple('Point', ['x', 'y'])

and initialized by adding parameters on instantiation::

	>>> point = Point(2,4)
	>>> point.x
	2

Namedtuples will not accept new attributes::

	>>> point.z = 6
	Traceback (most recent call last):
	  File "<stdin>", line 1, in <module>
	AttributeError: 'Point' object has no attribute 'z'

A similar goal can be achieved using :ref:`__slots__ <python slots>`.
