
Special methods
===============

Classes have many `special methods <https://docs.python.org/3/reference/datamodel.html#special-method-names>`_
that are invoked by special syntax (e.g. ``+`` or ``()``).
Those can be overloaded to have a certain behaviour.

.. note::

   Emulation should only be implemented to the level it makes sense.

E.g.::

    >>> class Character:
    ...     def __init__(self, name):
    ...         self.name = name
    ...
    >>> class Knight(Character):
    ...     __add__ = None
    ...
    >>> class Squire(Character): pass
    ...
    >>> class King(Character):
    ...     def __init__(self, name):
    ...         super().__init__(name)
    ...         self.knights = []
    ...         self.squires = []
    ...     def __add__(self, other):
    ...         if isinstance(other, Knight):
    ...             self.knights.append(other)
    ...         elif isinstance(other, Squire):
    ...             self.squires.append(other)
    ...     def __str__(self):
    ...         return '{} with {} knights and {} squires'.format(self.name, len(self.knights), len(self.squires))
    ...
    >>> bors = Knight('Sir Bors')
    >>> patsy = Squire('Patsy')
    >>> arthur = King('Arthur King of Camelot')


Special methods are called by special syntax::

    >>> arthur + patsy  # calls __add__
    >>> for squire in arthur.squires:
    ...     print(squire.name)
    ...
    Patsy

    >>> arthur + bors  # calls __add__
    >>> for knight in arthur.knights:
    ...     print(knight.name.upper())
    ...
    SIR BORS

The different special methods are also called by different functions::

    >>> print(arthur)  # calls __str__
    Arthur King of Camelot with 1 knights and 1 squires




Setting a method to None indicates that the operation is not available::

    >>> bors + patsy
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: 'NoneType' object is not callable

