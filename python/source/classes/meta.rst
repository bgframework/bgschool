
Metaclasses
-----------

Classes are used to create instances.
Instances can be dynamically modified to add (or delete or alter)
its members. E.g.::

    >>> class C: pass
    ...
    >>> i = C()
    >>> i.x = 1

In addition, classes can be modified affecting existing and new
instances. E.g.::

    >>> C.y = 2
    >>> i.y
    2

`Metaclasses <https://docs.python.org/3/reference/datamodel.html#metaclasses>`_
are used to create classes,
and in a similar way on how classes affect instances,
metaclasses can be used to modify classes.

.. important:: In the vast majority of the cases,
   you do not need metaclasses, most of the changes
   can be done in other ways like decorators.

By default, classes are created using :func:`type`,
so these two are equivalent::

    class C: pass

    C = type('C', (), {})


E.g.::

    >>> class MyList(list): pass
    ...
    >>> l = MyList()
    >>> l.append(7)
    >>> l.txt = 'This is my list'
    >>> l
    [7]
    >>> l.txt
    'This is my list'
    >>>
    >>> MyList = type('MyList', (list,), dict(txt='This is my list using type'))
    >>> l = MyList()
    >>> l.append(7)
    >>> l
    [7]
    >>> l.txt
    'This is my list using type'


:func:`type` can also be used to create metaclasses::

    >>> class Meta(type):
    ...     def __init__(cls, name, bases, nmspc):
    ...             super().__init__(name, bases, nmspc)
    ...             cls.uses_metaclass = lambda self : "Yes!"
    ...
    >>> class MyClass(metaclass=Meta): pass
    ...
    >>> i = MyClass()
    >>> i.uses_metaclass()
    'Yes!'

.. note:: Metaclasses firts element is not an instance but a class
   and that is why ``cls`` is used as first argument.




Find more details in: https://python-3-patterns-idioms-test.readthedocs.io/en/latest/Metaprogramming.html