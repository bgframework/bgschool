
Exceptions
==========


.. note:: If you are interested in how to handle and capture exceptions
   see the :ref:`corresponding section <exceptions>`.


You can define your own exceptions by inheriting from :class:`Exception`::

    >>> class MyError(Exception):
    ...     pass
    ...
    >>> raise MyError()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    __main__.MyError


These exceptions accept a message to be displayed as the error message::

    >>> raise MyError('info message')
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    __main__.MyError: info message

However, you can also make it explicit::

    >>> class MyError(Exception):
    ...     def __init__(self, msg):
    ...             self.message = msg
    ...
    >>> raise MyError('info message')
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    __main__.MyError: info message

