
.. index:: class; context manager

Context manager
===============

Context managers define the runtime context to be established when executing a with statement.
They are used for locking and unlocking resources, closing opened files...

To create a context manager only 2 methods are required: :meth:`__enter__` and :meth:`__exit__`
as mentioned in
`<https://docs.python.org/3/reference/datamodel.html#with-statement-context-managers>`_

Lets see an example::

    >>> class KillerJoke:  # https://www.youtube.com/watch?v=8I3zCQzZx68
    ...     def __init__(self):
    ...         self.hidden = True
    ...     def __enter__(self):
    ...         self.hidden = False
    ...         return self
    ...     def __exit__(self, exc_type, exc_value, traceback):
    ...         self.hidden = True
    ...     def tell(self):
    ...         if self.hidden:
    ...             print('harmless')
    ...         else:
    ...             print('jajajajajajjajaj')
    ...             print('--------------------------')
    ...
    >>> killer_joke = KillerJoke()
    >>> killer_joke.tell()
    harmless

.. https://www.youtube.com/watch?v=8I3zCQzZx68

Context managers can be opened with the ``with`` statement,
which also takes care of closing them::

    >>> with killer_joke as kj:
    ...     kj.tell()
    ...
    jajajajajajjajaj
    --------------------------
    >>> killer_joke.tell()
    harmless

