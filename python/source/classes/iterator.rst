

.. index:: class; iterator

.. _iterator class:

Iterators
=========

Most container objects can be looped over using :ref:`for <for>` statements. E.g.:

.. code-block:: python

    for i in [1, 2, 3]:
        ...

The for statement call :func:`iter` on the container object which returns
an iterator object (or :exc:`TypeError` if iteration is not supported).
The iterator object defines the method :meth:`__next__` which
access elements in the container one at each time. When there are no more
elements, a :exc:`StopIteration` exception is raised. The :meth:`__next__`
method can be called using the built-in function :func:`next`.


.. _iterator class example:

This means that for add the iterator behaviour to your classes, only the
:meth:`__iter__` and :meth:`__next__` methods are required. E.g.::

   >>> class Alphabet:
   ...     def __init__(self):
   ...         self.letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
   ...         self.pos = 0
   ...     def __iter__(self):
   ...         self.pos = -1
   ...         return self
   ...     def __next__(self):
   ...         self.pos += 1
   ...         if self.pos == len(self.letters):
   ...             raise StopIteration
   ...         else:
   ...             return self.letters[self.pos]
   ...
   >>> for letter in Alphabet():
   ...      print(letter, end=', ')
   ...
   A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,

See another example in: `<https://docs.python.org/3/tutorial/classes.html#iterators>`_.

Iterators can be materialized as lists or tuples by using the
:func:`list` or :func:`tuple` constructor functions,
or sequence unpacking::

   >>> L = [1,2,3]
   >>> iterator = iter(L)
   >>> t = tuple(iterator)
   >>> t
   (1, 2, 3)

   >>> iterator = iter(L)
   >>> a,b,c = iterator
   >>> a,b,c
   (1, 2, 3)


Related
-------

Functions often used with iterators:

.. hlist::
   :columns: 4

   * :func:`map`
   * :func:`filter`
   * :func:`enumerate`
   * :func:`sorted`
   * :func:`any`
   * :func:`all`
   * :func:`zip`

The :mod:`itertools` module contains a number of commonly-used iterators
as well as functions for combining several iterators.

