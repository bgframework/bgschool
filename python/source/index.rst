.. Python documentation master file, created by
   sphinx-quickstart on Fri Nov 10 10:08:14 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Python tutorial
===============

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   style
   interpreter
   keywords
   basicTypes
   flowControl
   functions
   modules
   IO
   exceptions
   scope_and_namespace
   classes
   generator
   regex
   decorators
   logging
   packages

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
