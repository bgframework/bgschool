
.. _decorators:

Decorators
==========

A decorator is a software `design pattern <https://en.wikipedia.org/wiki/Decorator_pattern>`_ that
alters the functionality of a function, method, or class
without explicitly modifying it.

Decorators provide a simple syntax for calling `higher-order functions <https://en.wikipedia.org/wiki/Higher-order_function>`_.


You can find more examples in https://realpython.com/blog/python/primer-on-python-decorators/
and https://www.thecodeship.com/patterns/guide-to-python-function-decorators/

Intro
-----

Before diving into decorators, make sure you understand how
:ref:`functions <functions>` work in Python.

In addition, we are going to review a few things
about functions:

.. https://www.youtube.com/watch?v=anwy2MPT5RE

- Functions are `first-class objects <http://python-history.blogspot.com.es/2009/02/first-class-everything.html>`_
  meaning that they can be assigned to variables::

      >>> def spam():
      ...     print('spam spam spam')
      ...
      >>> menu = spam
      >>> menu()
      spam spam spam

  and passed around (e.g. as function arguments)::


      >>> def call(f):
      ...     f()
      ...
      >>> call(spam)
      spam spam spam


- Nested functions are allowed so you can define functions inside functions::

      >>> def menu():
      ...     print('Menu')
      ...     def one():
      ...             print('egg and spam')
      ...     def two():
      ...             print('spam, spam, spam')
      ...     one()
      ...     two()
      ...
      >>> menu()
      Menu
      egg and spam
      spam, spam, spam

  .. warning::

     Functions defined in other functions are not directly callable::

        >>> one()
        Traceback (most recent call last):
          File "<stdin>", line 1, in <module>
        NameError: name 'one' is not defined


  .. note::

     Recall that nested functions have access to the enclosing :ref:`scope <scope>`::

        >>> def menu(dish):
        ...     def say():
        ...             print('Menu:' + dish)
        ...     say()
        ...
        >>> menu('spam')
        Menu:spam


- Functions can return functions::

      >>> def get_menu():
      ...     def menu():
      ...             print('spam and eggs')
      ...     return menu
      ...
      >>> f = get_menu()
      >>> f()
      spam and eggs


Basis
-----

A decorator is essentially a function that receives another function as parameter
and does something before and/or after calling the other.

Lets suppose we want to decorate this function::

  >>> def spam():
  ...     print('spam spam spam')
  ...

According to what we have just written, one might thing of a decorator as::

  >>> # THIS CODE IS WRONG
  >>> def decorator(f):
  ...     print('before')
  ...     f()
  ...     print('after')
  ...
  >>> decorator(spam)
  before
  spam spam spam
  after

However, that is not a decorator, it is simply a function that receives another and calls it.
The key is that the function we decorate **must return a function**::


  >>> def decorator(f):
  ...     def wrapper(*args, **kwargs):
  ...             print('before')
  ...             f(*args, **kwargs)
  ...             print('after')
  ...     return wrapper
  ...
  >>> decorated_spam = decorator(spam)
  >>> decorated_spam()
  before
  spam spam spam
  after

.. note::

   The wrapper receives ``*args`` and ``**kwargs``
   just to make it work with any function, independently
   of the parameters it needs. It is just a design choice
   that you can modify anytime.

.. warning::

   The function we are decorating does not return anything.
   If it was doing, we need to return its value within the ``wrapper``::


      >>> def decorator(f):
      ...     def wrapper(*args, **kwargs):
      ...             print('before')
      ...             r = f(*args, **kwargs)
      ...             print('after')
      ...             return r
      ...     return wrapper
      ...


Python introduces a *syntactic sugar* to easily use decorators.
By simply adding ``@`` plus the decorator function name
before the function definition, the function is decorated::

  >>> @decorator
  ... def spam():
  ...     print('spam spam spam')
  ...
  >>> spam()
  before
  spam spam spam
  after


Adding parameters
-----------------

Decorators can also receive arguments.
To do so, we need to pass them to the decorator, calling it.
Thus, this decorator function should **return a decorator**::

  >>> def inform_decorator(message):
  ...     def decorator(f):
  ...             def wrapper(*args, **kwargs):
  ...                     print(message)
  ...                     f(*args, **kwargs)
  ...             return wrapper
  ...     return decorator
  ...
  >>> @inform_decorator('Our menu contains')
  ... def spam():
  ...     print('spam spam spam')
  ...
  >>> spam()
  Our menu contains
  spam spam spam


Preserving function metadata
----------------------------

The *function metadata* is a set of attributes of a particular function
(e.g. ``__name__``, ``__doc__`` or ``__module__``).
Although most of the time you will not care about them,
they can be useful is some situations (e.g. debugging)
or even essential sometimes.

E.g.::

  >>> def spam():
  ...     print('spam spam spam')
  ...
  >>> spam.__name__
  'spam'

One of the drawbacks of decorators is that the change the metadata of the decorated function::

  >>> def decorator(f):
  ...     def wrapper(*args, **kwargs):
  ...             return f()
  ...     return wrapper
  ...
  >>> @decorator
  ... def spam():
  ...     return ('spam spam spam')
  ...
  >>> spam.__name__
  'wrapper'

Luckily, this problem can be easily tackled using the :func:`functools.wraps`
decorator before our wrapper::

  >>> import functools
  >>> def decorator(f):
  ...     @functools.wraps(f)
  ...     def wrapper(*args, **kwargs):
  ...             return f()
  ...     return wrapper
  ...
  >>> @decorator
  ... def spam():
  ...     print('spam spam spam')
  ...
  >>> spam.__name__
  'spam'




Examples
--------

Get the time that a function takes to run::

  >>> import time
  >>> def timeme(f):
  ...     def wrapper(*args, **kwargs):
  ...             t1 = time.time()
  ...             r = f(*args, **kwargs)
  ...             t2 = time.time()
  ...             print('Took {} to run it'.format(t2-t1))
  ...             return r
  ...     return wrapper
  >>> @timeme
  ... def create_squares_list(v):
  ...     l = list()
  ...     for i in range(v):
  ...             l.append(i**2)
  ...     return l
  ...
  >>> create_squares_list(5)
  Took 3.743171691894531e-05 to run it
  [0, 1, 4, 9, 16]
  >>> create_squares_list(10)
  Took 6.0558319091796875e-05 to run it
  [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]



Find more examples in https://wiki.python.org/moin/PythonDecoratorLibrary

