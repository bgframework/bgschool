
Modules
=======

A module is a file containing Python definitions and statements.
The file name is the module name with the suffix ``.py``  appended.

To use an item from a module in another, it must be imported::

   >>> #import only the module name to the current symbol table
   >>> import my_module
   >>> my_module.my_function()  # access function

::

   >>> #import all names the module defines (except the ones begging with '_')
   >>> from my_module import *  # not recommended
   >>> my_function()  # module name is not needed

::

  >>> #import selected names
  >>> from my_module import my_func
  >>> my_function()  # module name is not needed

You can use :func:`dir` to list which names a module defines::

   >>> import my_module
   >>> dir(my_module)
   ['__name__', 'my_function']

Use :func:`dir` without arguments to list the names you have defined currently.

:func:`dir` does not list built-in functions and variables.
They are defined in :mod:`builtins` standard module::

    >>> import builtins
    >>> dir(builtins)




Within a module, the module’s name (as a string) is available as the value of the global variable :data:`__name__`.

A module can contain executable statements as well as function definitions.
These statements are intended to initialize the module.
They are executed only the first time the module name is encountered in an import statement.
(They are also run if the file is executed as a script.)

.. index:: peculiar module; import once

.. warning::

    For efficiency reasons, each module is only imported once per interpreter session.
    Therefore, if you change your modules, you must restart the interpreter – or,
    if it’s just one module you want to test interactively, use :func:`importlib.reload`,
    e.g. ``import importlib; importlib.reload(modulename)``


Each module has its own private symbol table,
which is used as the global symbol table by all functions defined in the module.
Thus, the author of a module can use global variables in the module without worrying about accidental clashes
with a user’s global variables.


.. index:: peculiar module; executable

Module as script
----------------

Running a module with::

    $ python my_module.py <arguments>

the code will be executed with :attr:`__name__` as ``__main__``.

So you can make the module usable as script adding::

    if __name__ == "__main__":
        ...

This will not happen if the modules are just imported.

Module Search Path
------------------

When importing a module, the interpreter first search for a built-in module with that name.
If not found, it searches for a file named ``<mod_name>.py`` in the directories given in
:data:`sys.path`. This variable is initialized from:

    - The directory containing the input script (or the current directory when no file is specified).
    - ``PYTHONPATH`` (a list of directory names, with the same syntax as the shell variable PATH).
    - The installation-dependent default.

After initialization, Python programs can modify sys.path.
The directory containing the script being run is placed at the beginning of the search path,
ahead of the standard library path.
This means that scripts in that directory will be loaded instead of modules of the same name in the library directory.

The ``sys.path`` can be modified using list methods.

"Compiled" files
----------------

To speed up loading modules, Python caches the compiled version of each module
in the  ``__pycache__`` directory under the name ``module.version.pyc``.

See more details in `<https://docs.python.org/3/tutorial/modules.html#compiled-python-files>`_.

.. _py pkg:

Packages
--------

Packages are a way of structuring Python’s module namespace by using “dotted module names”.

A directory is treated as a package if it contains :file:`__init__.py` file.

:file:`__init__.py` can be empty or set the ``__all__`` variable or
execute package initialization code.

.. note::

    **__all__**

    When ``from package import *`` is encountered it does not import all submodules.
    To import certain submodules add them in a list assigned to ``__all__``.

    Details in `<https://docs.python.org/3/tutorial/modules.html#importing-from-a-package>`_.

.. note::

    Importing a sub-package executes the code in the :file:`__init__.py` file of the
    parent package and the subpackage.

    See `<https://docs.python.org/3/reference/import.html#regular-packages>`_.


.. _py pkg rel import:

Relative imports
****************

For packages with subpackages relative imports can be used::

    from . import echo

Relative imports are based on the name of the current module.
Since the name of the main module is always ``"__main__"``,
modules intended for use as the main module of a Python application must always use absolute imports.


In multiple directories
***********************

Packages support one more special attribute, :attr:`__path__`.


More details in `<https://docs.python.org/3/tutorial/modules.html#packages-in-multiple-directories>`_.
