
Coding style
============

Main points:

- Use 4-space indentation, no tabs.
- Wrap lines so that they don’t exceed 79 characters.
- Use blank lines to separate functions and classes, and larger blocks of code inside functions.
- When possible, put comments on a line of their own.
- Use docstrings.
- Use spaces around operators and after commas, but not directly inside bracketing constructs.
- Name your classes and functions consistently;
  the convention is to use ``CamelCase`` for classes and
  ``lower_case_with_underscores`` for functions and methods.
  Always use ``self`` as the name for the first method argument.
- Don’t use fancy encodings if possible.
- Likewise, don’t use non-ASCII characters in identifiers.

