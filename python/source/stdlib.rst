####################
The standard library
####################

Two useful built-in functions:

- :func:`dir`: returns a list of all module functions
- :func:`help`: returns an extensive manual page created from the module's docstrings


OS interface
============

- :mod:`os`

.. warning::

    Use ``import os`` style instead of ``from os import *``
    to prevent shadowing the built-in function :func:`open` os.open()
    with :func:`os.open`.

File and directory management tasks: :mod:`shutil`


File wildcards
==============

File list making from directory wildcard searches: :mod:`glob`

Command line arguments
======================

Command line arguments are stored in :attr:`argv` attribute
of the :mod:`sys` module as a list.

Command line argument processing:
    - :mod:`getopt`
    - :mod:`argparse` (more flexible)


Standard input and output
=========================

The :mod:`sys` module as attributes for *stdin*, *stdout* and *stderr*.

Output format
-------------

:func:`repr` version customized: :mod:`reprlib`

Printing control: :mod:`pprint`

Paragraph formatting of text to fit given screen widths: :mod:`textwrap`

Culture specific formats: :mod:`locale`

:class:`string.Template`

Logging
-------

:mod:`logging`

At simplest, log messages are sent to a file or to ``sys.stderr``

Program termination
===================

:func:`sys.exit`

Mathematics
===========

- Floating point: :mod:`math`
- Floating point where the user expects the results to match calculations done by hand: :class:`decimal.Decimal`
- Random selections: :mod:`random`
- Basic statistics properties calculation: :mod:`statistics`
- Numerical computations: `Scipy <https://scipy.org>`_


Internet
========

- Retrieving data from URLs: :mod:`urllib.request`
- Mail sending: :mod:`smtplib`
- Email messages management: :mod:`email`


Dates and times
===============

:mod:`datetime`

Data compression
================

- :mod:`zlib`
- :mod:`gzip`
- :mod:`bz2`
- :mod:`lzma`
- :mod:`zipfile`
- :mod:`tarfile`

Data parsing
============

JSON: :mod:`json`

Comma-Separated Value format: :mod:`csv`


XML:
    - :mod:`xml.etree.ElementTree`
    - :mod:`xml.dom`
    - :mod:`xml.sax`

SQLitle: :mod:`sqlite3`

Binary data: :mod:`struct`

Performance measurement
=======================

- :mod:`timeit`
- :mod:`profile`
- :mod:`pstats`

Testing
=======

Simple tests in docstrings: :mod:`doctest`

- :mod:`unittest`
- `Pytest <http://doc.pytest.org>`_


Internationalization
====================

- :mod:`gettext`
- :mod:`locale`
- :mod:`codecs`

Multi-threading
===============

:mod:`threading`

Synchronized objects for inter-thread communication and coordination: :mod:`queue`


Weak references
===============

Python does automatic memory management
(reference counting for most objects and garbage collection to eliminate cycles).
The memory is freed shortly after the last reference to it has been eliminated.

This approach works fine for most applications but occasionally
there is a need to track objects only as long as they are being used by something else.
Unfortunately, just tracking them creates a reference that makes them permanent.
The :mod:`weakref` module provides tools for tracking objects without creating a reference.
When the object is no longer needed, it is automatically removed from a weakref table and
a callback is triggered for weakref objects.
Typical applications include caching objects that are expensive to create.

Example in: `<https://docs.python.org/3/tutorial/stdlib2.html#weak-references>`_

Tools for lists
===============

C-style arrays: :mod:`array`.
List with homogeneous data can be stored more compactly.

Lists with faster appends and pops from the left side but slower lookups in the middle: :class:`collections.deque`

Manipulation of sorted lists: :mod:`bisect`

Heaps based on lists
(The lowest valued entry is always kept at position zero.
This is useful for applications which repeatedly access
the smallest element but do not want to run a full list sort): :mod:`heapq`

Others
======

- The :mod:`xmlrpc.client` and :mod:`xmlrpc.server` modules
  make implementing remote procedure calls into an almost trivial task.





