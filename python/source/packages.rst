
Packages
========


Creating a package for a Python project is easy,
however, there a different ways of doing it,
and following some common practices
(as the ones in https://packaging.python.org/tutorials/packaging-projects/)
might be a good idea.


In Python, the most common way to package and distribute your
project is using setuptools: https://packaging.python.org/guides/distributing-packages-using-setuptools/ .


The structure
-------------

The simplest structure for your project is::

   |- <project name>/
   |
   |  |- <project name>/
   |  |  |
   |  |  |- __init__.py
   |  |  |- main.py
   |
   |- setup.py



.. note::

   Although some other layouts are also suitable,
   e.g. having the code under a ``src`` directory
   (as in https://blog.ionelmc.ro/2014/05/25/python-packaging/#the-structure ),
   this layout is simple as does the work.

When creating source code for your package,
it is important to understand the function
of the :ref:`__init__.py <py pkg>` files
and when you can use :ref:`relative imports <py pkg rel import>`
and when you cannot.

In summary, any directory with an :file:`__init__.py` file
is treated as a Python package (or subpackage)
and you cannot use relative imports in
modules intended to be used as the main modules.


The setup.py
************

The :file:`setup.py` script is the most important part
when it comes to create a module.
It is the build script for `setuptools <https://packaging.python.org/key_projects/#setuptools>`_
and it can be used to install the project in different ways.
See the :ref:`distribution section <pkg distribution>` for more details.

A minimal :file:`setup.py` file looks like::

   from setuptools import setup, find_packages

   setup(
       name="<package name>",
       version="<package version>",
       packages=find_packages(),
   )


In that :func:`~setuptools.setup` function the arguments are:

:name: Name of your package (only letters, numbers, ``_`` and ``-`` are allowed).
:version: version of the package (normally: ``MAJOR.MINOR.BUGFIX``). Read more in the
  `PEP 404 <https://www.python.org/dev/peps/pep-0440/>`_.
:packages: all Python packages that should be included in the distribution.
  The :func:`~setuptools.find_packages` function can discover all packages and subpackages for you.

More fields can be added to the setup.
The most common ones are:

:description: Short description of your project (typically one line)
:url: URL of your repository
:author: Author's name
:author_email: Author's email address
:classifiers: some additional metadata of the package.
  Ideally, you should include the version(s) of Python your
  package works on, the license and the OS.
  Find all classifiers in https://pypi.org/classifiers/


More details of the setup script can be found in https://docs.python.org/3/distutils/setupscript.html


Adding more to your project
---------------------------

Adding a license
****************

Choosing the right license for your project is essential.
However, once this choose has been made, you should
indicate your license in you project using a
:file:`LICENSE` of :file:`LICENSE.txt` file::

   |- <project name>/
   |
   |  |- <project name>/
   |
   |- LICENSE.txt
   |- setup.py

In addition, the :file:`setup.py` script can be modified
to include it in the :func:`setup` function::

    setup(
       ...,
       license="<your license>",
       classifiers=(
           ...,
           "License :: <License classifier>",
       )
   )

Adding a readme
***************

The basic documentation that every project should have is the ``README`` file.
You can use plain text, markdown or reStructuredText to write,
but it is important to have it in your project.

The ``README`` file can also be included in the :file:`setup.py` script
as the long description of your project::

   with open('README.rst', encoding='utf-8') as f:
       long_description = f.read()


   setup(
       ...,
       long_description=long_description,
       long_description_content_type='text/x-rst'
   )



Adding data
***********

There are 2 ways to include data within your package distribution.

The first one is indicating those file in the `MANIFEST.in <https://docs.python.org/3.7/distutils/sourcedist.html#specifying-the-files-to-distribute>`_ file
and setting the **include_package_data** to :obj:`True` in the setup function::

   setup(
       ...,
       include_package_data=True
   )

This tells setuptools to install any data files it finds in your packages
and that are specified in the :file:`MANIFEST.in`.
This files contains a set of instructions of which files to include or exclude
in your distribution (find an example in https://www.reddit.com/r/Python/comments/40s8qw/simplify_your_manifestin_commands/ )

The second option is to use the **package_data** keyword
(which is  a dictionary that maps from package names to lists of glob patterns).
E.g.::

   setup(
       ...,
       package_data={
           # If any package contains *.txt or *.rst files, include them:
           '': ['*.txt', '*.rst'],
           # And include any *.msg files found in the 'hello' package, too:
           'hello': ['*.msg'],
       }
   )


Read more (including how to exclude data packages in http://setuptools.readthedocs.io/en/latest/setuptools.html#including-data-files )


Enabling CLI
************

To enable a command line interface for you package,
you only need to indicate under which name
you want to call your command line function
in the :file:`setup.py` file::

   setup(
       ...,
       entry_points={
        'console_scripts': [
            '<cli name> = <package>.<module>:<function>',
        ]
    }
   )




Managing dependencies
---------------------

The easiest way to indicate dependencies for your Python project
is to use the **install_requires** keyword in the setup function::

   setup(
    ...,
    install_requires=['A', 'B>=3.0', 'C>1,<2']
   )

Dependencies through a file
***************************

An exhaustive list of the project requirements is
typically included though a :file:`requirements.txt` file.
This alternative is used to enable the creation of
a similar environment to the one used to develop the package.
You can read more in https://packaging.python.org/discussions/install-requires-vs-requirements/

The :file:`requirements.txt` file can be similar to the
options in the **install_requires** keyword. E.g.::

   A
   B>=3
   C>1,<2

Using a :file:`requirements.txt` has some advantages if you are using environments.
You can use the :command:`pip freeze` command to generate this file,
and you can create environments (even conda environments) from it.

`Requirements files <https://pip.pypa.io/en/latest/user_guide/#requirements-files>`_
can be read in the setup script and passed to the **install_requires** keyword::


   with open('requirements.txt') as f:
       required = f.read().splitlines()


   setup(
    ...,
    install_requires=required,
   )

.. warning::

   The :file:`requirements.txt` file is not automatically included in your
   distribution. You need to explicitly indicate it in your :file:`MANIFEST.in` file.


Other folders
-------------

Documentation
*************

In addition to the ``README`` file, more documentation
can be added using `Spinhx <http://www.sphinx-doc.org/en/master/>`_
or other technical documentation tool.
Those files are typically included in a *doc* or *docs* folder.

With the documentation, your layout looks like::


   |- <project name>/
   |
   |  |- <project name>/
   |
   |  |- docs/
   |
   |- README.rst
   |- setup.py


Tests
*****

If you want to add some testing to your project,
it is a good idea to use a framework that helps
you to do it like `pytest <https://docs.pytest.org/en/latest/>`_,
`nose <https://nose.readthedocs.io/en/latest/>`_ or `unittest <https://docs.python.org/3/library/unittest.html>`_.

Regardless of the framework you use, it is a good idea to have your tests under a
*test* folder::


   |- <project name>/
   |
   |  |- <project name>/
   |
   |  |- test/
   |
   |- setup.py


Structure summary
-----------------

If you include all the above mentioned options, the structure will end up in something
similar to::


   |- <project name>/
   |
   |  |- <project name>/
   |  |  |
   |  |  |- __init__.py
   |  |  |- main.py
   |
   |  |- docs/
   |
   |  |- tests/
   |
   |- LICENSE.txt
   |- MANIFEST.in
   |- README.rst
   |- requirements.txt
   |- setup.py


Your :file:`MANIFEST.in` will look like::

   # Files to be included in the distribution
   include requirements.txt
   include LICENSE
   include README.rst



And your :file:`setup.py` will be::

   from setuptools import setup, find_packages


   # Get requirements from the requirements.txt file
   with open('requirements.txt') as f:
       required = f.read().splitlines()


   # Get the long description from the README file
   with open('README.rst', encoding='utf-8') as f:
       long_description = f.read()


   setup(
       name='<package name>",
       version="<package version>",
       description="<package short description>",
       long_description=long_description,
       long_description_content_type="text/x-rst",
       url="<package URL>",
       author="<you>",
       author_email="<your email address>",
       license="<package license>",
       packages=find_packages(),
       install_requires=required,
       entry_points={
           'console_scripts': [
               '<cli name> = <package name>.<mod>:<func>',
           ]
       }
   )



.. _pkg distribution:

Distributing your package
-------------------------

Probably, the best way to distribute your package is using
`the Python Package Index <https://pypi.org/>`_.

However, for a fast distribution, you can use
the **sdist** command to create a source distribution:

.. code:: bash

   python setup.py sdist

Check the different formats in https://docs.python.org/3.7/distutils/sourcedist.html#creating-a-source-distribution

More details in https://docs.python.org/3.7/distributing/index.html
and  https://docs.python.org/3.7/distutils/index.html
