
Agora
======



**Ellipsis**

**Interpreter**

    - Quiz (x1)
    - Last expression
    - User prompts
    - Exit with CTR+D

**Booleans**
    - Quiz (x3)
    - False values
    - Operators

**Dict, list & set**
    - Quiz (x1)
    - List comprehension & list multiplication
    - Dict comprehension
    - Set operations

**String**
    - Quiz (x1)
    - Join
    - Format & template

**Flow**
    - Quiz (x2)
    - Pass
    - Enumerate
    - Reversed
    - Zip
    - Chain comparisons
    - Else in for loop

**Functions**
    - Quiz (x4)
    - Operator as functions
    - Lambda expressions
    - Default values
    - Unpacking arguments
    - Annotations
    - Generators

**Classes**
    - Quiz (x2)
    - Name mangling
    - Instance




Kahoot
------

E.g.::

        >>> 1+1
        2
        >>> ?
        2


::

   >>> bool([])
   ?



::

   >>> bool(None)
   ?


::

    >>> '' or 'a' or 'b'
    ?


::

    >>> [[]] *3


E.g. ::

    >>> for i in range(10):
    ...     pass
    ... else:
    ...     print('What am I doing in here?')
    ...



E.g. ::

    >>> a, b, c = 0, 1, 1
    >>> a < b == c



E.g. ::

    >>> def f(a=[]):
    ...     if len(a) == 0:
    ...             return 'empty'
    ...     else:
    ...             return 'not empty'
    ...
    >>> f()



E.g. ::

    >>> def f(a, b, *args, c=0, **kwargs):
    ...     return a + b + sum(args) + c + sum(kwargs.values())
    ...
    >>> f(1,2,3,c=7,d=8)


::

    >>> first, *rest, last = (1,2,3,4)
    >>> rest



::

    >>> def f():
    ...     return 1
    ...


E.g. ::

    >>> class A:
    ...     def f(self):
    ...             return 7
    ...

