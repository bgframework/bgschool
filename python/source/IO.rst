
Input and output
================

3 ways to get output:
    #. Write to file
    #. Expression statements
    #. Use :func:`print`

Output format
-------------

Values to string
****************

Values can be converted to strings using these functions:
:func:`str` and :func:`repr`.

:func:`str`
   Used to create output for the end user, its goal is to be readable::

      >>> print(str('Hello world'))
      Hello world
      >>> print(str(3))
      3
      >>> print(str('3'))
      3

:func:`repr`
   Is the "official" string representation and its goal is to unambiguous::


      >>> print(repr('Hello world'))
      'Hello world'
      >>> print(repr('3'))
      '3'
      >>> print(repr(3))
      3


Formating
*********

Output can be formatted by yourself,
providing a :obj:`string.Template` or
using :meth:`str.format`.

By yourself
^^^^^^^^^^^

:obj:`str` objects have different functions to format the output:
    - justify (:meth:`str.ljust`, :meth:`str.center`, :meth:`str.rjust`)
    - add zeros (:meth:`str.zfill`)


.. index:: string; format

str.format()
^^^^^^^^^^^^

The :meth:`str.format` replaces '{}'
with objects passed to the method::

    >>> print('We are the {} who say "{}!"'.format('knights', 'Ni'))
    We are the knights who say "Ni!"

Numbers refer to the position, and keyword arguments are possible::

    >>> print('{1}, {0} and {other}'.format('Bill', 'Matt', other='you'))
    Matt, Bill and you

The values can be converted:
    - ``!a`` apply :func:`ascii`
    - ``!s`` apply :func:`str`
    - ``!r`` apply :func:`repr`

An optional ``':'`` and format specifier can follow the field name::

    >>> import math
    >>> print('The value of PI is approximately {0:.3f}.'.format(math.pi))
    The value of PI is approximately 3.142.

    >>> print('The value of PI is approximately {pi:.3f}.'.format(pi=math.pi))
    The value of PI is approximately 3.142.

Passing an integer after the ``':'`` will cause that field to be a minimum number of characters wide.

.. index:: string; template

string template
^^^^^^^^^^^^^^^

The :obj:`string.Template` is similar to the :meth:`str.format` in the sense that some keys are
replaced by the provided values. However, there are differences. The main two differences are:
values should be passed in a dict-like format and you can perform partial substitutions::

   >>> import string
   >>> template = string.Template('$who likes $what')
   >>> template.safe_substitute({'who': 'Sir Gallahad'})
   'Sir Gallahad likes $what'


Old style
^^^^^^^^^

The ``%`` operator can also be used for string formatting.
It interprets the left argument much like a
`sprintf()-style <https://docs.python.org/3/library/stdtypes.html#printf-style-string-formatting>`_::

    >>> import math
    >>> print('The value of PI is approximately %5.3f.' % math.pi)
    The value of PI is approximately 3.142.



Files
-----

:func:`open` returns a file object.
Usually it takes 2 arguments: ``open(filename, mode)``.

.. table:: mode options

    ==========  ===================================
    ``'r'``     read (default)
    ``'w'``     write (existing files are deleted)
    ``'a'``     append
    ``'r+'``    read and write
    ==========  ===================================

Normally files are opened in **text mode**: strings in specific encoding
(the default encoding is platform dependant). In this mode, by default,
platform specific line endings (``'\n'`` or ``'\r\n'``) are converted to
``'\n'`` when reading and the other way round when writing.

To read data as byte objects (for files not containing text) use the
**binary mode** appending ``'b'`` to the mode.

File object methods
*******************

read([size])
    read a file ([some quantity of data]) and returns it. Returns empty string at the end of the file.

    .. warning::

        - Negative sizes is equal to no size.
        - User must handle files larger than machine memory.

readline()
    read one line from the file. New line characters are left at the end.
    Returns empty string at the end of the file.

    .. note::

        File lines can also be read directly from the file object::

            >>> for line in f:
            ...     print(line, end='')

readlines()
    read all the lines of a file

        .. hint::

            Alternative::

                >>> list(f)

write(<something>)
    writes <something> to a file. Returns the number of characters written (for stings).
tell()
    in *binary mode* returns the object’s current position
    as number of bytes from the beginning
seek(offset, [from_what])
    in *binary mode* change file position adding an offset to a reference point (indicated by *from_what*)

    .. table:: reference point

        ====    ===================
        0       beggining (default)
        1       current position
        2       end
        ====    ===================

close()
    free up any resources taken by the open file.

With
****

It is good practice to use the :ref:`with <with>` keyword when dealing with file objects.
The file is properly closed after its suite finishes, even if an exception is raised::

    >>> with open('file', 'r') as f:
    ...     read_data = f.read()

Useful modules
**************

JSON
^^^^

The module :mod:`json` can take Python data hierarchies,
and convert them to string representations; this process is called *serializing*.

Main methods are:
    - :meth:`json.dumps` to view the JSON representation,
    - :meth:`json.dump` to serialize to a file
    - :meth:`json.load` to deserialize

Serializing lists and dictionaries is straightforward.
For arbitrary classes, extra effort is required.

Pickle
^^^^^^

:mod:`pickle` is a protocol which allows the serialization of arbitrarily complex Python objects.
It is also insecure by default:
deserializing pickle data can execute arbitrary code.
