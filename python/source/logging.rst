
Logging
=======

Logging is important for system developing, debugging and running.
When a program crashes, if there is no logging record,
you have little chance to understand what happened.

Some people argue that a disciplined developer who uses logging and testing should hardly need an interactive debugger.

Why not printing?
-----------------

Using ``print`` statements is a common and easy way to show information
through the command line. However, the only time that print is a better option than logging
is when the goal is to display a help statement for a command line application.

On the other hand, using the ``logging`` library gives some more advantages
like silencing certain messages or setting levels for the messages.
In addition, it can help developers to provide a context for the information
while in a print statement the context needs to be manually added.

Take a look at the section `when to use logging <https://docs.python.org/3/howto/logging.html#when-to-use-logging>`_
of the Python documentation.



Quick start
-----------

There are several tutorials on the internet, including
`a basic tutorial <https://docs.python.org/3/howto/logging.html#logging-basic-tutorial>`_
for the logging module in Python.

In short, a **message** to be loggged log consist on the *text* to be logged
and the *level of severity* of such message.
The Python logging module already defines certain `levels of severity <https://docs.python.org/3/library/logging.html#logging-levels>`_
(``CRITICAL``, ``ERROR``, ``WARNING``, ``INFO`` and ``DEBUG``).
There is a function for each of these levels.
For example, to get a warning message::

    >>> logging.warning('Message')
    WARNING:root:Message

As you can see, there are 3 items in the output text:

- ``WARNING`` is the level of severity of the message
- ``root`` is the logger name
- ``Message`` is our text

As mentioned there are different level of severity already defined::

    >>> logging.critical('Critical message')
    CRITICAL:root:Critical message
    >>> logging.info('Information')
    >>>

By default, the level is set to ``WARNING``.
This means that  only events of this level and above will be tracked.

To also log messages of lower levels, we need to configure the logging module.
E.g. is we set the level to minimum (which is ``DEBUG``) all messages will
be logged::

    >>> logging.basicConfig(level=logging.DEBUG)
    >>> logging.info('Information')
    INFO:root:Information

.. attention:: The call to :func:`~logging.basicConfig` **must** be done
   before any logging call.

The :func:`~logging.basicConfig` function is simple but still powerful.
For example, you can change the output of the logging from the console to
a file easily::

    >>> logging.basicConfig(filename='example.log', level=logging.DEBUG)

.. hint:: Add ``filemode='w'`` as keyword argument if you want to file to
   be created each time.

One last thing that you can use with this function is to change
the format of the message.
For example, we can only print the level and message::

    >>> logging.basicConfig(format='%(levelname)s -- %(message)s')
    >>> logging.warning('Message')
    WARNING -- Message

Or even add the time::

    >>> logging.basicConfig(format='%(asctime)s -- %(message)s')
    >>> logging.warning('Message')
    2018-10-02 16:19:08,682 -- Message


Finally, it is important to mention that you only need to configure the
logging once, and it will work for all modules, packages...

For example, if you create a script (:file:`myscript.py`) with
a function that logs a message::

    # myscript.py
    import logging

    def f():
        logging.info('I am the f function')

And then you can see it::

    >>> import logging
    >>> import myscript
    >>> logging.basicConfig(level=logging.DEBUG)
    >>> myscript.f()
    INFO:root:I am the f function



Concepts
--------

To further understand how logging works, it is important to know
which are the building blocks:

- :ref:`Log record <logging msg>`: contains not only the message to be logged
  but also additional information such as the time or the *level of severity*.
- :ref:`Logger <logging logger>`: exposes the interface that the application code
  uses.
- :ref:`Handler <logging handler>`: sends the log records to the appropriate
  destination.
- :ref:`Filter <logging filter>`: makes a particular logger or handler reject
  a particular log record.
- :ref:`Formatter <logging fmt>`: configures the final order, structure, and
  contents of the log message


The ways those building blocks work together is as follows
(see in `flow diagram <https://docs.python.org/3.5/howto/logging.html#logging-flow>`_):
First, the application calls a :ref:`logger <logging logger>`
to log a particular message. If the logger is enabled for the level of
that message, then a :ref:`log record <logging msg>` object is created.
Afterward, if the logger has :ref:`filters <logging filter>` attached,
it is checked whether the message passes them or not.
When there are not filters, or the message passes them, it is send
to the :ref:`handler(s) <logging handler>` of the logger (if any).
Each handler checks whether the log record has the appropriate level
and if it passed the filters of that handler (if any).
If all conditions are met, the handler :ref:`formats <logging fmt>` the message
and sends it to the desired destination.
When the handlers have finished their job and, only if *propagation*
is enabled (which is by default), the log record is passed to the
parent logger, which passes it to the handlers and the process
continues.

This process has several implications that can be found in the sections
of each of the building blocks:

.. contents::
   :local:
   :depth: 1



.. _logging msg:

Log record
**********

Each log contains a message (the information developers want to indicate).
Thus, the message is key::

    >>> logging.warning('Message')
    WARNING:root:Message

However, the object the the logging module receives is not a simple string
but a :class:`~logging.LogRecord` instance, that contains not only
message, but information about the level, time...

Check all the attributes in https://docs.python.org/3/library/logging.html#logrecord-attributes

Message can be *unformed* strings that are created using ``msg % args``::

    >>> logging.warning('%s', 'Message')
    WARNING:root:Message

Two important things to know are:

- if an object is passed, the :meth:`__str__` is used to create the message::

        >>> class Record:
        ...     def __str__(self):
        ...             return 'Message'
        ...
        >>> logging.warning('%s', Record())
        WARNING:root:Message


  This can be used to create messages using other styles like
  :class:`string.Template` or :meth:`str.format`
  as explained in https://docs.python.org/3/howto/logging-cookbook.html#use-of-alternative-formatting-styles
  and https://docs.python.org/3/howto/logging-cookbook.html#using-custom-message-objects

- formatting of message arguments is deferred until it cannot be avoided.
  However, this does not mean that computing the arguments is also deferred.
  Check https://docs.python.org/3/howto/logging.html#optimization for more details.



.. _logging logger:

Logger
******

Loggers objects expose the interface for the application code:

    >>> logger = logging.getLogger()
    >>> logger.warning('Message')
    WARNING:root:Message

In fact, using the functions of the module (:func:`logging.warning` ...)
is the same as calling the **root** logger
(which is the same as you get when you call :func:`logging.getLogger()`).

The most basic configuration for a logger is to set
a minimum `level of severity <https://docs.python.org/3/howto/logging.html#logging-levels>`_
for the messages::

    >>> logger.debug('Message')
    >>>

.. note::

   Nothing has been printed because the default level of severity
   is ``WARNING``.

In addition, a logger object can have :ref:`handlers <logging handler>`
and :ref:`filters <logging filter>`.

More details in https://docs.python.org/3/howto/logging.html#loggers

Structure
^^^^^^^^^

Loggers are a hierarchical structure with the ``root`` logger on top.
In can be retrieved with no arguments calling :func:`~logging.getLogger`::

    >>> root_logger = logging.getLogger()


From it, the structured is creating using ``.`` to separate levels.
For example ``foo.bar`` is descendant of ``foo``.
Each call to :func:`~logging.getLogger` with the same name, returns
the same logger::

    >>> logger1 = logging.getLogger('foo')
    >>> logger2 = logging.getLogger('foo')
    >>> logger1 is logger2
    True

.. note::

   A good convention to use when naming loggers is to use a module-level logger, named as follows::

      logger = logging.getLogger(__name__)

.. important:: The level of a particular logger can be set explicitly.
   If it not set, it inherits its parents level. The default level for
   the root logger is ``WARNING``.

Child loggers propagate messages up to the handlers associated with their ancestor loggers.
Thanks to this, it is sufficient to configure handlers for a top-level logger and create child loggers as needed
(propagation can be turned off setting the *propagate* attribute to ``False``).

.. note::

   When propagation is enabled, parent loggers accept any message from their descendants,
   regardless of the level of severity. For example, if ``foo`` logger only accepts
   ``ERROR`` and more severe messages, but ``foo.bar`` also accepts ``INFO`` messages,
   any ``INFO`` messages from ``foo.bar`` will be propagated to ``foo`` that will
   give it to its handlers.

E.g.::

    >>> import logging
    >>> logging.basicConfig(format='%(message)s', level='WARNING')  # Configure the logging
    >>> logging.info('Hola')
    >>> # root logger does not accept info messages
    ...
    >>> logger = logging.getLogger('foo')  # Get a child logger
    >>> logger.setLevel(logging.INFO)  # Set the level to a lower one
    >>> logger.info('Message')
    Message




.. _logging handler:

Handler
*******

Handler objects dispatch the appropriate messages to the handler's destination.

The logging library includes some `useful handlers <https://docs.python.org/3/howto/logging.html#useful-handlers>`_.

Similar to :ref:`loggers <logging logger>` handlers can have different filters, and even a levels of severity.
A log record first needs to pass the logger level and then, it is only passed to the handler's destination if
it is allowed according to the level of severity of the handler.

.. warning::

   The logger level of severity is checked first. A log record that does not pass the logger level,
   will be never passed to handler even if it allows those messages.


In addition, handlers can also have a :ref:`formatter <logging fmt>`.

.. _logging filter:

Filter
******

Filters provide a finer grained facility for determining which log records to output.
They can be used by both, :ref:`loggers <logging logger>` and :ref:`handlers <logging handler>`.
There is a class :class:`~logging.Filter`, but filters can also be functions.

E.g.::

    class InfoFilter(logging.Filter):
        def filter(self, rec):
            return rec.levelno in (logging.DEBUG, logging.INFO)

Read more in https://docs.python.org/3/library/logging.html#filter-objects


.. _logging fmt:

Formatter
*********

:class:`~logging.Formatter` is an object that configures the final structure of
the log message.
In addition to the message, it can be used to add other
`attributes <https://docs.python.org/3/library/logging.html#logrecord-attributes>`_
of the :class:`~logging.LogRecord` to the final message.

E.g., if we add a formatter like::

    '%(asctime)s - %(levelname)s - %(message)s'

to a particular handler, a log message will be displayed as::

   2005-03-19 15:10:26,620 - INFO - Message text


Some interesting attributes to use in the formatter are:

- ``%(message)s``: actual message passed
- ``%(asctime)s``: time. The format of the datetime can be configured when creating
  the log record. The default is ``%Y-%m-%d %H:%M:%S``
- ``%(levelname)s``: level
- ``%(name)s``: name of the logger. When using ``__name__`` as the logger name it can be really helpful
  to know where the log came from.




Using the logging
-----------------

The most basic usage of the logging system is to get the logger and call
the appropriate functions. E.g.::

    >>> logging.getLogger(__name__).warning('Message')
    WARNING:__main__:Message

This is how to set up all your message. Remember to use the appropriate function according to the level.
All the rest, should be part of the :ref:`configuration <logging conf>`.


.. _logging conf:

Configuration
-------------

Configuration of the logging module should come before any call
to the methods like :func:`~logging.debug`, :func:`~logging.info`...
In fact, `incremental configuration <https://docs.python.org/3/library/logging.con>`_
might introduce some problems.

The easiest way to configure the logging module is to use
the :func:`~logging.basicConfig` function::

    >>> logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
    >>> logging.debug('This is a debug message')
    DEBUG:This is a debug message


.. note::

   This function adds a StreamHandler and the default formatter
   (``severity:logger name:message``) to the root logger.
   This function does nothing if the root logger already has handlers configured for it.
   Is is called by the  :func:`logging.debug`, :func:`logging.info` and similar functions.

.. important:: If you do not configure the logger at all
   (not using :func:`~logging.basicConfig` or calling :func:`logging.debug`...)
   then you won't be able to use other loggers properly. E.g.::

        >>> import logging
        >>> logger = logging.getLogger('foo')
        >>> logger.setLevel(logging.INFO)
        >>> logger.info('Message')  # Nothing is printed
        >>> logger.warning('Message')  # Printed without any format
        Message


Using :func:`~logging.basicConfig` can be an easy way to configure the module, but for more complex configurations
use a file or dictionary as shown in https://docs.python.org/3/library/logging.config.html#module-logging.config
Check the schema in https://docs.python.org/3/library/logging.config.html#dictionary-schema-details
https://docs.python.org/3/library/logging.config.html#configuration-file-format

E.g::

    >>> import logging
    >>> import logging.config
    >>> logging_conf = {
    ...     'version': 1,
    ...     'formatters': {
    ...         'simple': {
    ...             'format': '%(asctime)s %(levelname)s -- %(message)s'
    ...         }
    ...     },
    ...     'handlers': {
    ...         'console': {
    ...             'class': 'logging.StreamHandler',
    ...             'formatter': 'simple',
    ...             'level': 'NOTSET',
    ...             'stream': 'ext://sys.stdout'
    ...         }
    ...     },
    ...     'loggers': {
    ...         'mylogger': {
    ...             'level': 'INFO'
    ...         }
    ...     },
    ...     'root': {
    ...         'level': 'WARNING',
    ...         'handlers': ['console']
    ...     }
    ... }
    >>> logging.config.dictConfig(logging_conf)
    >>> logger = logging.getLogger('mylogger')
    >>> logger.info('Using my logger')
    2017-11-27 14:12:53,056 INFO -- Using my logger





.. warning::

   Take care with :func:`~logging.config.dictConfig` and :func:`~logging.config.fileConfig`
   as they disable exiting loggers by default.
   If this is not the behaviour you want, you can change it.

If an application uses `ConfigObj <http://www.voidspace.org.uk/python/configobj.html>`_
then the configuration can be read as a dictionary from the configuration file
and it can even add some modifications before applying it to the logging module.


Configuration for a library
****************************

When writing a library, you should never add handlers and configure
your loggers although it is really needed and you document well how to use it.
It should be the final users, in his/her application the one who
configures the logging system.

The only thing you should do, is to add a :class:`~logging.NullHandler`
to the main logger in the main :file:`__init__.py` as in the
`requests library <https://github.com/requests/requests>`_.
In your main :file:`__init__.py`::

    import logging
    logging.getLogger(__name__).addHandler(logging.NullHandler())

Doing this, you prevent your library to log messages in the absence of
any configuration.

For example, let assume the library is a :file:`myscript.py` file::

    # myscript.py
    import logging
    logger = logging.getLogger(__name__)

    def f():
        logger.info('Info message')
        logger.warning('Warning message')

If no configuration is set for the logging, warning and above
messages will be printed out.::

    >>> import myscript
    >>> myscript.f()
    Warning message
    >>>

Adding a :class:`~logging.NullHandler`::

    # myscript.py
    import logging
    logger = logging.getLogger(__name__)
    logger.addHandler(logging.NullHandler())

    def f():
        logger.info('Info message')
        logger.warning('Warning message')

you prevent them::

    >>> import myscript
    >>> myscript.f()
    >>>



Advanced
--------

Context managers can be used for selective logging:
https://docs.python.org/3/howto/logging-cookbook.html#using-a-context-manager-for-selective-logging


The logging predefined levels (``DEBUG``, ``INFO``...) are useful,
specially because loggers (and the logging module itself) include a function for each.
However, if you want, you can define your own custom levels and use
the :meth:`~logging.Logger.log` method with a numeric level.
However, it might `not be a good idea <https://docs.python.org/3/howto/logging.html#custom-levels>`_.

Remarks
-------


- Use module-level logger (following the conventions)::

      logger = logging.getLogger(__name__)


- Do not get the logger at module level::

      logger = logging.getLogger(__name__)

      def foo():
          logger.info('Hi, foo')

  Because the logger can be created before the configuration
  of a parent is set, and thus, it does not have the expected configuration.
  Read more in https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/

  Use::

      def foo():
          logger = logging.getLogger(__name__)
          logger.info('Hi, foo')

  However, if you set :code:`disable_existing_loggers=False` while
  configuring logging (by default it is set to :code:`True`)
  it should be save to get loggers at module level.


- Avoid creating the string before calling the method, and leave that
  to the logs module::

      logger.warning('%s', 'Message')

  If you prefer to use :class:`string.Template` or :meth:`str.format`
  check https://docs.python.org/3/howto/logging-cookbook.html#use-of-alternative-formatting-styles


- Do not change the configuration of your logging all over the place.
  Do it once, in the mean thread.

- Parent loggers with a certain level can accept messages from a lower level
  if the descendant is configured for accepting them.

- Try to avoid logging during module initialization.

- Although it exists a function to explicitly capture logs from exceptions
  (:meth:`~logging.Logger.exception`) consider its use carefully.
  To get this logs, you need to capture the exception, and if you are capturing
  an exception is probably to manage it somehow and you do not want an
  error message for something you are already managing.
  In addition, this log records have ``ERROR`` level, so they cannot be silence
  without silencing lower levels also.

Links
-----


- `Module docs <https://docs.python.org/3/library/logging.html#module-logging>`_


Source for this notes:

- `Basic tutorial <https://docs.python.org/3/howto/logging.html#logging-basic-tutorial>`_
- `Advanced tutorial <https://docs.python.org/3/howto/logging.html#logging-advanced-tutorial>`_
- `Cookbook <https://docs.python.org/3/howto/logging-cookbook.html#logging-cookbook>`_
- http://docs.python-guide.org/en/latest/writing/logging
- https://www.loggly.com/blog/4-reasons-a-python-logging-library-is-much-better-than-putting-print-statements-everywhere/
- https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/
- https://www.digitalocean.com/community/tutorials/how-to-use-logging-in-python-3
- https://www.loggly.com/ultimate-guide/python-logging-basics/
- https://opensource.com/article/17/9/python-logging
- http://pythonsweetness.tumblr.com/post/67394619015/use-of-logging-package-from-within-a-library


Some libraries on top of the logging module:

- `pygogo <https://github.com/reubano/pygogo>`_
- `daikiri <http://daiquiri.readthedocs.io/en/latest/#>`_
