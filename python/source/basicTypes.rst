
Some basic types and operations
===============================

Some built-in constants
-----------------------

:obj:`None`
   Frequently represents the absence of a value

:obj:`NotImplemented`
   Special value returned by binary special methods (e.g. :meth:`~object.__eq__` or :meth:`~object.__add__`)
   to indicate that the operation is not implemented.

   .. note::

      Although it seems similar to :exc:`NotImplementedError` they are not interchangeable.

.. index:: peculiar basic; ellipsis

:obj:`Ellipsis`
   Same as ``...``

Boolean
-------

Boolean values are represented by 2 constant values:
   - :obj:`False`
   - :obj:`True`

In numeric contexts they behave like 0 and 1.

The built in function :func:`bool` can be used to convert values to Boolean.

.. index:: boolean; false values

Truth value testing
*******************

The following are considered as false:


- :obj:`None`
- :obj:`False`
- Zero of any numeric type (0, 0.0, 0j)
- Empty sequences (``''``, ``()``, ``[]``)
- empty mappings (``{}``)
- Instances of classes whose :meth:`~object.__bool__` or :meth:`~object.__len__` methods return zero or False

.. _boolean operators:

Operations
**********

In descending priority:

- :ref:`not <not>`
- :ref:`and <and>`
- :ref:`or <or>`

.. z``and`` and ``or`` are so-called *short-circuit* operators:
   their arguments are evaluated from left to right,
   and evaluation stops as soon as the outcome is determined.

.. index:: boolean; operators

- The Boolean operators ``and`` and ``or`` are so-called *short-circuit* operators:
  their arguments are evaluated from left to right,
  and evaluation stops as soon as the outcome is determined.
  For example, if A and C are true but B is false, A and B and C does not evaluate the expression C.

- When used as a general value and not as a Boolean,
  the return value of a *short-circuit* operator is the last evaluated argument:

   .. code-block:: python

      >>> string1, string2, string3 = '', 'Trondheim', 'Hammer Dance'
      >>> string1 or string2 or string3
      'Trondheim'

   Which can be really useful. E.g. ::

      >>> b = a if a is not None else c

   can be replaced by::

      >>> b = a or c


.. _boolean comparisons:

Comparisons
***********

Operations:

.. hlist::
   :columns: 5

   * ``<``
   * ``>``
   * ``<=``
   * ``>=``
   * ``==``
   * ``!=``
   * ``is``
   * ``is not``
   * ``in``
   * ``not in``



- Use ``in`` and ``not in``
  to check whether a value occurs (does not occur) in a sequence.

- Use ``is`` and ``is not`` to compare whether two objects are really the same object;
  this only matters for mutable objects like lists.

Objects of different types, except different numeric types, never compare equal.
Non-identical instances of a class normally compare as non-equal unless the class defines the :meth:`~object.__eq__` method.

Instances of a class cannot be ordered with respect to other instances of the same class,
or other types of object, unless the class defines enough of the
methods :meth:`~object.__lt__`, :meth:`~object.__le__`,
:meth:`~object.__gt__`, and :meth:`~object.__ge__`.
The behavior of the ``is`` and ``is not`` operators cannot be customized.



Numbers
-------

Basic numeric types are:


- :obj:`int`: integers
- :obj:`float`: floats


Information about the precision and internal representation of floating point numbers
for the machine on which your program is running is available in :data:`sys.float_info`.

Other types are available:

- :obj:`decimal.Decimal`: floating point where the user expects the results to match calculations done by hand.
  Offers advantages compared to :obj:`float`
- :obj:`fractions.Fraction`
- :obj:`complex`

Operations
**********

.. hlist::
   :columns: 5

   * ``+``
   * ``-``
   * ``*``
   * ``/``
   * ``//``
   * ``%``
   * :func:`divmod`
   * ``-x``
   * :func:`abs`
   * :func:`int`
   * :func:`float`
   * :func:`complex`
   * :func:`conjugate`
   * :func:`pow`
   * ``**``

All :class:`numbers.Real` types also include:

.. hlist::
   :columns: 4

   * :func:`math.trunc`
   * :func:`round`
   * :func:`math.floor`
   * :func:`math.ceil`


Special cases
^^^^^^^^^^^^^

- Divisions (``/``) always return float.
- Floor divisions [#floordiv]_ (``//``) return integer.
- Powers (``**``) have higher precedence than ``-`` [#powers]_
- Some float precision issues can be tackled with the already
  mentioned :mod:`decimal` and :mod:`fractions` modules, but
  there are also other useful tools:

     - :meth:`float.as_integer_ratio`: express a float as division of integers.
     - :meth:`float.hex`: express a float in hexadecimal (exact value stored
       by the computer). Useful for porting.
     - :func:`math.fsum`: helps mitigate loss-of-precision during summation.

.. [#floordiv] Discarding any fractional result
.. [#powers] ``-3**2`` is equivalent to ``-(3**2)``



Bitwise operations on integers
******************************

.. hlist::
   :columns: 6

   * ``|``
   * ``^``
   * ``&``
   * ``<<``
   * ``>>``
   * ``~``

Strings
-------

Use single quotes (``'...'``) or double quotes (``"..."``) [#strings]_.

Use ``\`` to escape quotes.

If you don’t want characters prefaced by ``\`` to be interpreted as special characters,
you can use raw strings by adding an r before the first quote::

   >>> print(r'C:\some\name')
   C:\some\name

String literals can span multiple lines. One way is using triple-quotes: ``"""..."""`` or ``'''...'''``.
End of lines are automatically included in the string,
but it’s possible to prevent this by adding a ``\`` at the end of the line.

Strings can be concatenated with the ``+`` operator, and repeated with  ``*``.

Two or more *string literals* next to each other are automatically concatenated::

   >>> 'Py' 'thon'
   'Python'

.. index:: string; join

This feature can be used to join several strings together by putting them into a parenthesis::

    >>> text = ('Py'
                'thon')
    >>> text
    'Python'

.. _string prefixes:

Strings can be preceded by one of this 4 prefixes:

.. hlist::
   :columns: 4

   * r
   * u
   * R
   * U

Example::

   >>> path = r'C:\some\name'


Special features
****************

- Strings are arrays, so they can be indexed.
- String are `inmutable <https://docs.python.org/3/glossary.html#term-immutable>`_.


Useful functions
****************

Some useful functions:

:meth:`str.lower`
   return a lowercase copy

:meth:`str.upper`
   return an uppercase copy

:meth:`str.replace`
   return a copy where all occurrences of certain substring are replaced by another

:meth:`str.split`

:meth:`str.splitlines`

:meth:`str.translate`


.. [#strings] Unlike other languages, special characters such as ``\n`` have the same meaning with both single and double quotes.
   The only difference between the two is that within single quotes you don’t need to escape ``"``
   (but you have to escape ``\'``) and vice versa.


Sequences
---------

- Strings are also immutable sequences, but they have been addressed already.
- Concatenating immutable sequences always results in a new object.

Common operations
*****************


.. hlist::
   :columns: 5

   * ``in``
   * ``not in``
   * ``+``
   * ``*``
   * ``[i [:j [:k]]``
   * :func:`len`
   * :func:`min`
   * :func:`max`
   * ``index(x[, i[, j]])``
   * ``count(x)``

Lists
*****

- Lists can be indexed.
- List can be sliced, but slicing results in a new list (shallow copy).
   - The :meth:`list.copy` method is equivalent to ``l[:]`` (shallow copy).
- Lists are mutable
- List work fine as LIFO using :meth:`list.append` and :meth:`list.pop`.
   - List are not efficient as FIFO. Use :obj:`collections.deque` for this purpose.

Items can be removed using :ref:`del <del>`.

.. index:: comprehension; list

List comprehensions
^^^^^^^^^^^^^^^^^^^

List comprehensions provide a concise way to create lists.

A list comprehension consists of brackets containing an expression followed by a for clause,
then zero or more for or if clauses.

Example::

   >>> [(x, y) for x in [1,2,3] for y in [3,1,4] if x != y]
   [(1, 3), (1, 4), (2, 3), (2, 1), (2, 4), (3, 1), (3, 4)]

Is equivalent to::

   >>> combs = []
   >>> for x in [1,2,3]:
   ...     for y in [3,1,4]:
   ...         if x != y:
   ...             combs.append((x, y))
   ...
   >>> combs

List comprehensions allow nesting::

   >>> [[row[i] for row in matrix] for i in range(4)]
   [[1, 5, 9], [2, 6, 10], [3, 7, 11], [4, 8, 12]]

.. hint::

   Matrix transpose::

      >>> list(zip(*matrix))


.. index:: peculiar basic; lists multiplication

.. warning::

   When using ``*`` for lists::

      >>> lists = [[]] * 3
      # the empty list is REFERENCED 3 times
      >>> lists[0].append(3)
      >>> lists
      [[3], [3], [3]]

Tuples
******

A :obj:`tuple` consists of a number of values separated by commas.

They may be input with or without surrounding parentheses::

   >>> t = 12345, 54321, 'hello!'

- Tuples are immutable.
- Usually contain a heterogeneous sequence of elements
- Access to elements is done via
   - unpacking
   - indexing

Special cases:

- Empty tuples::

      >>> t = ()

- 1 element tuple::

      >>> t = 'hello',    # <-- note trailing comma

- :obj:`collections.namedtuple`

**Unpacking** can be done easily::

   >>> t = 12345, 54321, 'hello!'
   >>> x, y , z = t


Ranges
******

The :class:`range` type represents an immutable sequence of numbers
and is commonly used for looping a specific number of times in for loops::

   >>> for i in range(7):
   ...     pass

Binary
******

Binary data can be manipulated with the built-in types:
:class:`bytes` and :class:`bytearray`.

:class:`memoryview` can be used to access the memory of binary objects.

:mod:`array` supports efficient storage of basic data types.

Bytes
^^^^^

Syntax of byte literals is the same as :ref:`string literals <string prefixes>` with ``b`` prefix added;
e.g. ``b'example'``.

.. note::

   All possible prefixes:

   .. hlist::
      :columns: 5

      * b
      * B
      * br
      * Br
      * bR
      * BR
      * rb
      * rB
      * Rb
      * RB


Only ASCII characters are permitted in bytes literals.

Bytes objects actually behave like immutable sequences of integers,
with each value in the sequence restricted such that 0 <= x < 256.


Bytearray
^^^^^^^^^

:class:`bytearray` objects are a mutable counterpart to bytes objects.



Sets
----

A set is an unordered collection with no duplicate elements.

Curly braces or the :obj:`set` function can be used to create sets.
Empty sets can be created with ``set()``.

Example::

   >>> basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
   >>> print(basket)                      # show that duplicates have been removed
   {'orange', 'banana', 'pear', 'apple'}


.. index:: peculiar basic; sets operations

Set objects also support mathematical operations like union, intersection, difference, and symmetric difference.

Example::

   >>> a = set('abracadabra')
   >>> b = set('alacazam')
   >>> a                                  # unique letters in a
   {'a', 'r', 'b', 'c', 'd'}
   >>> a - b                              # letters in a but not in b
   {'r', 'd', 'b'}
   >>> a | b                              # letters in either a or b
   {'a', 'c', 'r', 'd', 'b', 'm', 'z', 'l'}
   >>> a & b                              # letters in both a and b
   {'a', 'c'}
   >>> a ^ b                              # letters in a or b but not both
   {'r', 'd', 'b', 'm', 'z', 'l'}

Similarly to list comprehensions, set comprehensions are also supported.

Example::

   >>> a = {x for x in 'abracadabra' if x not in 'abc'}


The immutable counterpart of :obj:`set` is :obj:`frozenset`.

Dictionaries
------------

Dictionareis are an unordered set of key: value pairs,
with the requirement that the keys are unique.

Dictionaries are indexed by keys, which can be any immutable type:
strings, numbers, Tuples...
Tuples can be used as keys if they contain only strings, numbers, or tuples;
if a tuple contains any mutable object either directly or indirectly, it cannot be used as a key.
You can’t use lists as keys.

- Use ``{}`` to create an empty dictionary.
- It is also possible to delete a key:value pair with :ref:`del <del>`.
- If you store using a key that is already in use, the old value associated with that key is forgotten.
- It is an error to extract a value using a non-existent key.
- To return a list of all the keys used in the dictionary: ``list(d.keys())``
- To return a sorted list of all the keys used in the dictionary: ``sorted(d.keys())``
- To check whether a single key is in the dictionary, use the :ref:`in <in>` keyword.


Example::

   >>> tel = {'jack': 4098, 'sape': 4139}
   >>> tel['jack']  # get one value
   4098
   >>> tel['guido'] = 4127 # add one value
   >>> tel
   {'sape': 4139, 'guido': 4127, 'jack': 4098}

The :obj:`dict` constructor builds dictionaries directly from sequences of key-value pairs::

   >>> dict([('sape', 4139), ('guido', 4127), ('jack', 4098)])
   {'sape': 4139, 'jack': 4098, 'guido': 4127}


When the keys are simple strings, it is sometimes easier to specify pairs using keyword arguments::

   >>> dict(sape=4139, guido=4127, jack=4098)
   {'sape': 4139, 'jack': 4098, 'guido': 4127}

.. index:: comprehension; dict

Dict comprehensions can be used to create dictionaries from arbitrary key and value expressions::

   >>> {x: x**2 for x in (2, 4, 6)}
   {2: 4, 4: 16, 6: 36}

