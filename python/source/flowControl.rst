
Flow control
============

Conditionals
------------

:ref:`If <if>` - [:ref:`elif <elif>`] - [:ref:`else <else>`] statements::

   >>> if a:
   ...     do(1)
   ... elif b:
   ...     do(2)
   ... else:
   ...     do(3)


``If-elif`` can be used as replacements for ``switch-case`` statements.

More on conditions
******************

- The conditions used in ``while`` and ``if`` statements can contain any operators,
  not just :ref:`boolean comparisons <boolean comparisons>`.

- All comparison operators have the same priority, which is lower than that of all numerical operators.
  Boolean operators have lower priority than comparison operators. Between them ``not`` has the highest
  and ``or`` the lowest.

.. index:: peculiar flow; chain comparisons

- Comparisons can be chained. For example, ``a < b == c``.

- In Python, unlike C, assignment cannot occur inside expressions.


.. _conditional operators:

Operators
*********

The operators that can be used are boolean :ref:`operators <boolean operators>` and :ref:`comparisons <boolean comparisons>`.

- Use ``in`` and ``not in``
  to check whether a value occurs (does not occur) in a sequence.

- Use ``is`` and ``is not`` to compare whether two objects are really the same object;
  this only matters for mutable objects like lists.- Use ``in`` and ``not in``
  to check whether a value occurs (does not occur) in a sequence.

- Use ``is`` and ``is not`` to compare whether two objects are really the same object;
  this only matters for mutable objects like lists.

- Boolean operators (``and`` and ``or``) can be used in comparisons and ``not`` to negate Boolean expressions.





Loops
-----

While
*****

:ref:`While loops <while>`  are used for repeated execution as long as an expression is true::

   >>> while ismet(condition):
   ...    do()


Conditions are evaluated as in an if clause.

For
****

:ref:`For <for>` statements iterate over the items of any sequence (a list or a string),
in the order that they appear in the sequence:

   >>> for element in list_:
   ...    do()


.. index:: peculiar flow; for loop else

Continue, break and else
************************

:ref:`break <break>`
     Breaks out of the smallest enclosing for or while loop.
:ref:`continue <continue>`
    Continues with the next iteration of the loop
:ref:`else <else>`
    It is executed when the loop terminates through exhaustion of the list (with for)
    or when the condition becomes false (with while),
    but not when the loop is terminated by a break statement.

    Example: `<https://docs.python.org/3/tutorial/controlflow.html#break-and-continue-statements-and-else-clauses-on-loops>`_.




Modifications
*************

If you need to modify the sequence you are iterating over while inside the loop,
it is recommended that you first make a copy.
Iterating over a sequence does not implicitly make a copy.

For example, if you are iterating through a list, you can use the slice copy to create a copy of the list::

   >>> my_list[:]  # creates a copy of the list

Range function
**************

The :func:`range` can be used to iterate over sequences of numbers::

    range([start], stop, [step])

This function does not return a list, it yields the values to save space.
If you need a list::

    >>> list(range(10))


Techniques
----------

Loop to pairs key-value of dictionaries using the the items() method::

    >>> knights = {'gallahad': 'the pure', 'robin': 'the brave'}
    >>> for k, v in knights.items():
    ...     print(k, v)
    ...
    gallahad the pure
    robin the brave


.. index:: peculiar flow; enumerate

When looping through a sequence, the position index and corresponding value can be retrieved at the same time
using the :func:`enumerate` function::

   >>> for i, v in enumerate(['tic', 'tac', 'toe']):
    ...     print(i, v)
    ...
    0 tic
    1 tac
    2 toe

.. index:: peculiar flow; zip

To loop over two or more sequences at the same time,
the entries can be paired with the :func:`zip` function::

    >>> questions = ['name', 'quest', 'favorite color']
    >>> answers = ['lancelot', 'the holy grail', 'blue']
    >>> for q, a in zip(questions, answers):
    ...     print('What is your {0}?  It is {1}.'.format(q, a))
    ...
    What is your name?  It is lancelot.
    What is your quest?  It is the holy grail.
    What is your favorite color?  It is blue.

.. index:: peculiar flow; reversed

To loop over a sequence in reverse, first specify the sequence in a forward direction
and then call the :func:`reversed` function::

    >>> for i in reversed(range(1, 10, 2)):
    ...     print(i)
    ...
    9
    7
    5
    3
    1

To loop over a sequence in sorted order,
use the :func:`sorted` function which returns a new sorted list while leaving the source unaltered::

    >>> basket = ['apple', 'orange', 'apple', 'pear', 'orange', 'banana']
    >>> for f in sorted(set(basket)):
    ...     print(f)
    ...
    apple
    banana
    orange
    pear


.. index:: peculiar flow; pass

Pass statement
--------------

:ref:`Pass <pass>` statement does nothing (null operation).

Comparing Sequences and Other Types
-----------------------------------

Sequence objects may be compared to other objects with the same sequence type.
The comparison uses *lexicographical* ordering:
first the first two items are compared,
and if they differ this determines the outcome of the comparison;
if they are equal, the next two items are compared, and so on,
until either sequence is exhausted.
If two items to be compared are themselves sequences of the same type,
the lexicographical comparison is carried out recursively.
If all items of two sequences compare equal, the sequences are considered equal.
If one sequence is an initial sub-sequence of the other,
the shorter sequence is the smaller (lesser) one.
Lexicographical ordering for strings uses the Unicode code point number to order individual characters.

::

   [1, 2, 3] < [1, 2, 4]
   True

Note that comparing objects of different types with ``<`` or ``>`` is legal
provided that the objects have appropriate comparison methods.
