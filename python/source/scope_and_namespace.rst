
.. _scope:

Python Scopes and Namespaces
============================

Attribute
    In this section attribute refers to any name
    following a dot. Examples:

    ================    ============================================================
    ``z.real``          ``real`` is an atrribute of object ``z``
    ``mod.funcname``    ``funcname`` is an attribute reference of the module ``mod``
    ================    ============================================================

    Attributes may be read-only or writable (assignment is possible, and deletion may be).

Namespace
    A namespace is a mapping from names to objects. In Python, most namespaces are
    implemented as dictionaries [#namespaceDict]_. There is no relation between names in different namespaces.

    Namespaces have different lifetimes. For example, the global namespace for a module
    is created when teh module definition is read in, and last till the interpreter quits.

    Statements executed by the top-level invocation of the interpreter are consider
    part of a module called :mod:`__main__`, so they have their own global namespace.

    The local namespace for a function is created when the function is called
    and deleted when the function returns.

Scope
    A scope is a textual region of a Python program where a namespace is directly accessible:
    an unqualified reference to a name attempts to find it in the namespace.

    Scopes are determined statically and used dynamically. The global scope
    of a function in a module is that module's namespace, no matter where is was
    called.

At any time during execution, there are at least 3 namespaces directly accessible
(ordered by search order):

    - innermost scope: local names
    - the scopes of any enclosing functions (searched starting with the nearest enclosing scope):
      non-local, but also non-global names
    - the next-to-last scope: current module’s global names
    - the outermost scope: built-in names

When reading a variable, if it is not found in the innnermost scope,
it search upwards until it is found.


*Assignments do not copy data — they just bind names to objects.*

Assignments (and deletions) to names go into the innermost scope.
This behaviour can be modified to refer to
names in the outside scope using :ref:`nonlocal <nonlocal>` and
:ref:`global <global>`.

Outside functions, the local scope references the same namespace as the global scope: the module’s namespace.
Class definitions place yet another namespace in the local scope.


Example
-------

.. https://docs.python.org/3/tutorial/classes.html#scopes-and-namespaces-example

.. code-block:: python

    def scope_test():
        def do_local():
            spam = "local spam"

        def do_nonlocal():
            nonlocal spam
            spam = "nonlocal spam"

        def do_global():
            global spam
            spam = "global spam"

        spam = "test spam"
        do_local()
        print("After local assignment:", spam)
        do_nonlocal()
        print("After nonlocal assignment:", spam)
        do_global()
        print("After global assignment:", spam)

    scope_test()
    print("In global scope:", spam)

Output:

.. code-block:: none

    After local assignment: test spam
    After nonlocal assignment: nonlocal spam
    After global assignment: nonlocal spam
    In global scope: global spam

.. [#namespaceDict] Module objects have a secret read-only attribute
    called :attr:`__dict__` which returns the dictionary used to implement
    the module’s namespace; the name :attr:`__dict__` is an attribute
    but not a global name.

