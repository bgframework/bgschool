
Errors and exceptions
=====================

Syntax errors
-------------

Also known as parsing errors.

The parser repeats the offending line and
displays a little ‘arrow’ pointing at the earliest point in the line where the error was detected.

E.g.::

   >>> d={'key: value}
      File "<stdin>", line 1
       d={'key: value}
                  ^
   SyntaxError: EOL while scanning string literal


.. _exceptions:

Exceptions
----------

Exceptions are errors during execution.
When an exception occurs, it is passed upwards in the code until it is
caught or it gets to the highest level and the program stops.


Standard exception names are `built-in identifiers <https://docs.python.org/3/library/exceptions.html#built-in-exceptions>`_.

.. index:: exceptions; handle

Handling
********

Exceptions can be handle with ``try-except`` statements::

    try:
        # clause (set of statements)
    except ExceptionType as this_exception:  # assing the exception to variable
        # exception type statements
    except (ExceptionType_1, ExceptionType_2): # catch different exceptions
        # other exception statements
    except: # catch all other exceptions
        # statements for all other exceptions
    else: # if not exception occurs
        # else clause

- If any of the statements in the try-clause produces an exception, the
  rest are skipped. Otherwise, the except clause(s) is(are) skipped.
- If the exception (or its base class) matches any exception type in the except clauses,
  the exception statements are executed.
  Otherwise, the exception is raised.
- Exceptions can be assigned to a variable in the except clause using ``'as'``.
- Many except statements are allow for different exceptions.
- The ExceptionType can contain a parenthesized tuple with multiple exceptions.
- The ExceptionType can be emtpy as wildcard for all exceptions.
- At most one handler will be executed.
- An else statement can be use for code executed only if no exception is raised.


Some exceptions have an associated value: **argument**; stored in
``instance.args``. Normally, the :meth:`__str__` method
writes those (but can be overwritten)::

    >>> try:
    ...     raise Exception('holy', 'grail')
    ... except Exception as inst:
    ...     print(inst.args)     # arguments stored in .args
    ...     print(inst)          # __str__ allows args to be printed directly,
    ...                          # but may be overridden in exception subclasses
    ...
    ('holy', 'grail')
    ('holy', 'grail')

Raise
*****

The :ref:`raise <raise>` statement allows to force some
exception to occur. Only  an exception instance or
an exception class (a class that derives from :class:`Exception`)
can be risen. E.g.::

    >>> raise NameError('HiThere')

Raise within a except clause produces a re-raise of
the exception. E.g.::

    >>> try:
    ...     raise NameError('HiThere')
    ... except NameError:
    ...     raise  # re-raises the exception



User-defined
************

Common practices:

- New classes derive from :class:`Exception`.
- Keep simple, only offering a number of attributes
  that allow information about the error to be extracted by handlers for the exception.
- When creating a module that can raise several distinct errors,
  create a base class for exceptions defined by that module,
  and subclass that to create specific exception classes for different error conditions.
- Use names that end in “Error”.

Clean-up actions
****************

Try statements have an optional **finally** clause
that is executed under all circumstances. It is
useful for releasing external resources.

Unhandled exceptions are re-raised after the finally clause
have been executed.

If an exception occurs during the during an
except or else clause, the finally clause is also executed.

.. code-block:: python

    >>> def divide(x, y):
    ...     try:
    ...         result = x / y
    ...     except ZeroDivisionError:
    ...         print("division by zero!")
    ...     else:
    ...         print("result is", result)
    ...     finally:
    ...         print("executing finally clause")
    ...


.. index:: exceptions; assertions

Assertions
----------

Assertions are a systematic way to check that the internal state of a program is as the programmer expected,
with the goal of catching bugs, mainly conditions that *should never happen*.
In particular, they're good for catching false assumptions that were made while writing the code,
or abuse of an interface by another programmer.

Basically, an assertion checks a condition and raises and exception
when it is not met. An optional message can be included in the exception::

    >>> assert 2 + 2 == 5, "Houston we've got a problem"
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    AssertionError: Houston we've got a problem

One of the key features of assetions, is that they can be "removed" [#assertionremove]_
by requesting optimization (using the ``-O`` option in the command line).

More details in https://wiki.python.org/moin/UsingAssertionsEffectively
and https://docs.python.org/3/reference/simple_stmts.html#assert

.. [#assertionremove] The compiler does not produce any code for assertions
   `__debug__ <https://docs.python.org/3/library/constants.html#__debug__>`_
   is ``False``.
