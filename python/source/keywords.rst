
Lexical analysis
================

Keywords
--------

The following identifiers are reserved in Python
and cannot be used as ordinary identifiers:

.. hlist::
    :columns: 4

    * False
    * None
    * True
    * and
    * as
    * assert
    * break
    * class
    * continue
    * def
    * del
    * elif
    * else
    * except
    * finally
    * for
    * from
    * global
    * if
    * import
    * in
    * is
    * lambda
    * nonlocal
    * not
    * or
    * pass
    * raise
    * return
    * try
    * while
    * with
    * yield


Identifiers
-----------

``__*__``
    system defined names
``__*``
    :ref:`class private name <private members>`

.. note::

    ``_`` (single underscore)
        Special meaning in the interactive shell,
        as it stores the result of the last evaluation

