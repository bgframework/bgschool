
The Python interpreter
======================

The Python interpreter is in charge of
translating Python commands to machine
instructions for their execution.

It can run in either *interactive* and *non-interactive* mode.
The first is when the commands are read from a **tty**.
To enter the interpreter:

.. code-block:: bash

    python

Exit the interpreter::

    >>> quit()


.. index:: peculiar interpreter; exit

Or type the end-of-line character: :code:`Ctrl-D` (:code:`Ctrl-Z` on Windows).

.. index:: peculiar interpreter; last expression

While in *interactive* mode,
you can refer to the last printed expression using ``_``::

   >>> 1 + 1
   2
   >>> 1 + _
   3

.. warning::

   This variable should be treated as read-only by the user. Don’t explicitly assign a value to it —
   you would create an independent local variable with the same name masking the built-in variable with its magic behavior.

.. index:: peculiar interpreter; prompts

When the interpreter is in *interactive* mode, the primary and secondary prompts ('>>> ' and '... ')
are defined as :data:`sys.ps1` and :data:`sys.ps2`.

----

You can also pass a script and arguments to run the interpreter in
*non-interactive* mode.
The script name and additional arguments thereafter are turned into a list of strings and assigned
to the :obj:`argv` variable in the :mod:`sys` module.
More details in: `<https://docs.python.org/3/tutorial/interpreter.html#argument-passing>`_.

.. note::

    Find more details in the `command line section of the Python documentation
    <https://docs.python.org/3/using/cmdline.html#command-line>`_


Code encoding
==============

By default, Python source files are treated as encoded in UTF-8.

It is also possible to specify a different encoding for source files. In order to do this,
put one more special comment line right after the ``#!`` line to define the source file encoding::

    # -*- coding: encoding -*-

The list of possible encodings can be found in the Python Library Reference, in the section on
`codecs <https://docs.python.org/3/library/codecs.html#module-codecs>`_.
