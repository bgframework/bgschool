Code
====

No rule is written in stone.
However, following a standard for coding helps to improve
quality, consistentcy, readability, compatibality and flexability
and therefore is higly recommended.
Some general good practices when writing code are:

- structure your code: split your code in reasonable groups (files and folders, packages, modules, classes, functions...)
- comment and document
- DRY (Don't Repeat Yourself)
- avoid deep nesting
- do refactoring
- always code as if the person who ends up maintaining your code is a violent psychopath who knows where you live
