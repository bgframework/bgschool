
Running in parallel
===================

Using parallel computation in a way to speed up your computations.
However, you must be aware of the kind of paralellization
you can use and the kind of improvement you can get
because it is not always worth.

Shared memory parallel execution is one of the easiest ways
to speed up your computations.
In Python, it can be as simple as using the
:class:`multiprocessing.Pool`.
E.g.::

from multiprocessing import Pool

def f(x):
    return x*x

with Pool(4) as pool:
