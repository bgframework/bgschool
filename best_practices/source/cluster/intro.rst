
BBGLab cluster
==============

Our cluster is a shared space where we all work,
so it is important that we all share some common good practices.

Beside the good practices, remember always to be respectful
with your colleagues:

- Do not take the whole cluster, even if the usage is low,
  specially when you are running long lasting jobs.
  Leave some room for the others.


.. toctree::
   :maxdepth: 2
   :caption: Tips:

   data
   jobs
   parallel


Extra tips
----------

The use of **screen** is quite powerful.
However, we have noticed a huge usage of screen
and a command recorder.
A better approach will be to write your commands to a file.
In addition, in our cluster each screen has its own history
kept by screen name. If you create a new screen with the same
name, you will be able to recover the history of that screen.